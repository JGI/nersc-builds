#!/bin/bash

battery() {
  pct=`pmset -g batt | grep InternalBattery | awk '{ print $3 }' | sed -e 's$%;$$'`
}

pause() {
  docker pause `docker ps --quiet --all --filter status=running` 2>/dev/null
}

unpause() {
  docker unpause `docker ps --quiet --all --filter status=paused` 2>/dev/null
}

last=999
while true
do

  battery
  echo "`date`: Battery level: $pct"

  if [ "$pct" -lt "90" ]; then
    if [ $last -ge "90" ]; then
      echo "Battery getting low..."
      pause
    fi
    last=$pct
  fi
  if [ "$pct" -gt "92" ]; then
    if [ $last -le "92" ]; then
      echo "Battery high enough to run..."
      unpause
    fi
    last=$pct
  fi

  sleep 60

done
