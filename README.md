# README #

This is where I'm tracking my build scripts for NERSC s/w

Best practices, to enforce:
- Use -fgcc-record-switches when using gcc 5.x or higher
  - use 'readelf -p .GCC.command.line $exe' to see what was used to build $exe
- Use make check
- Use "cd $(dirname $0)"
- Set PREFIX=$(cd ..; pwd)
- Use module purge, load all modules with explicit versions

- Clear the MODULEPATH, set it explicitly
- set PATH and LD_LIBRARY_PATH explicitly
- clear Perl and Python environment variables

Still do to:
- script to make sure all products are built, i.e. that when I add something new I don't forget to stuff it into the Dockerfile and into the build-everything script etc.