#%Module1.0
##

### This is a very special modulefile
### It reads the version to load from the filesystem
### Furthermore it reads its dependencies from the filesystem as well

## Required internal variables

set             name            bbtools
set             version         dev
set             baseroot        /usr/common/jgi/utilities/$name

proc check_version {baseroot version} {
	set lversion INVALID
    catch { if {$::env(BBTOOLS_VERSION != $version} { set lversion $::env(BBTOOLS_VER) } }
    catch { set lversion $::env(BBTOOLS_SEL) }

	if { $lversion == "INVALID" } {
        catch {
		    set ver_file "$baseroot/.modptr_$version"
		    set fd [open $ver_file]
		    set bytes [gets $fd line] 
		    if { $bytes > 0 } {
			    set lversion $line
		    }
		    close $fd
        }
	}
	return $lversion
}

proc get_deps {baseroot version} {
    set dep_file "$baseroot/$version/.deps"
    set deps {}
    if [file exists $dep_file] {
        catch {
            set fd [open $dep_file]
            while {[gets $fd line] >= 0} {
                lappend deps [string trim $line]
            }
            close $fd
        }
    }
    return $deps
}


set version_selection [check_version $baseroot $version]
set root /usr/common/jgi/utilities/$name/$version_selection

## List conflicting modules here
set mod_conflict [list $name]

## List prerequisite modules here
set deps [get_deps $baseroot $version_selection]
if {[llength $deps] == 0} {
    set deps {}
}
set mod_prereq_autoload [list PrgEnv-gnu gnustuff samtools fqzcomp]
set mod_prereq [list PrgEnv-gnu gnustuff samtools fqzcomp]

## Required for SVN hook to generate SWDB entry
set             fullname        bbtools
set             externalurl     https://bitbucket.org/berkeleylab/jgi-bbtools
set             nerscurl        https://bitbucket.org/berkeleylab/jgi-bbtools
set             maincategory    applications
set             subcategory     "bioinformatics"
set             description     "A collection of Bestus Bioinformatius tools, including BBMap (shord read aligner) and BBNorm (normalizer/error-corrector)"

## Source the common modules code-base
source /usr/common/usg/Modules/include/usgModInclude.tcl

## Software-specific settings exported to user environment
prepend-path    PATH            $root/bin
setenv          BBTOOLS_DIR      $root
setenv          BBTOOLS_VERSION $version
setenv          BBTOOLS_VER     $version_selection
