#!/bin/bash

set -ex
cd `dirname $0`

function buildit {
	prefix=$1
	tarball=$2
	configopts=$3
	dir=`echo $tarball | awk -F/ '{ print $NF }' | sed -e 's%.orig.tar.gz%%' -e 's%_%-%'`

	export LDFLAGS="-L$prefix/lib"
	export CPPFLAGS="-I$prefix/include"
	ORIG_LD_LIBRARY_PATH=$LD_LIBRARY_PATH

	[ -e $dir ] && rm -rf $dir

	tar xf $tarball
	pwd
	ls -l
	cd $dir
	./configure $configopts --prefix=$prefix
	make -j 8
	make install
	cd ..
	rm -rf $dir
}

function build_library {
	prgenv=$1
	prgenv_version=$2
	version=$3
	module purge
	module load PrgEnv-$prgenv/$prgenv_version
	PREFIX=$(cd ../..; pwd)/$prgenv$prgenv_version/$version
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
	[ -d $PREFIX ] && rm -rf $PREFIX

	buildit $PREFIX sources/libibverbs*
	buildit $PREFIX sources/libibcm*
	buildit $PREFIX sources/libibumad*
	buildit $PREFIX sources/libibmad*
	buildit $PREFIX sources/libmlx4*
	buildit $PREFIX sources/libmlx5*
    	buildit $PREFIX sources/librdmacm*
	fix_perms -g usg $PREFIX
}

echo "I give up, I can't find how to download the sources..."
exit 0

vsn=4.0-2.0.0.1
file=MLNX_OFED_SRC-debian-${vsn}.tgz
url=http://www.mellanox.com/downloads/ofed/MLNX_OFED-${vsn}/MLNX_OFED_SRC-${vsn}.tgz
dir=MLNX_OFED_SRC-${vsn}
[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir
[ -d sources ] && rm -rf sources

tar xf $file
mv $dir/SOURCES sources
rm -rf $dir

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_library gnu $env ${vsn}-Mellanox
done
create-module $PREFIX
