#!/bin/bash
set -ex
cd `dirname $0`

function build_openmpi {
	compiler=$1
	compiler_ver=$2
	version=$3
	subversion=$4
	sched=$5
	module purge
	module load PrgEnv-$compiler/$compiler_ver
	module list
	PREFIX=$(cd ..; pwd)/$compiler$compiler_ver/$sched/$version/$subversion
	[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
	[ -d $PREFIX ] && rm -rf $PREFIX

	MCA_NO_BUILD="--enable-mca-no-build=carto,crs,filem,routed-linear,snapc,pml-dr,pml-crcp2,pml-crcpw,pml-v,pml-example,crcp,pml-cm"
    DISABLE_OOB=0
	CONFIG_OTHER=""
	tar xf openmpi-$version.tar.*z*
	which gcc
	if [ $subversion == "ib" ]; then
		GP_INFINIBAND=1 module load OFED/2.0-2.0.5-Mellanox
		CONFIG_OTHER="--with-openib=${OFED_DIR}"
	fi
	if [ $subversion == "ib_2.0-3.0.0" ]; then
		GP_INFINIBAND=1 module load OFED/2.0-3.0.0-Mellanox
		CONFIG_OTHER="--with-openib=${OFED_DIR}"
	fi
	if [ $subversion == "ib_2.1-1.0.0" ]; then
		GP_INFINIBAND=1 module load OFED/2.1-1.0.0-Mellanox
		CONFIG_OTHER="--with-openib=${OFED_DIR}"
	fi
	cd openmpi-$version && \
	./configure --with-$sched --prefix=$PREFIX --enable-mpirun-prefix-by-default --disable-dlopen --disable-mem-debug --disable-mem-profile --disable-debug-symbols --enable-binaries --disable-heterogeneous --disable-debug --enable-shared --disable-static --disable-memchecker --disable-ipv6 --enable-mpi-f77 --enable-mpi-f90 --enable-mpi-cxx --enable-mpi-cxx-seek --disable-cxx-exceptions --enable-ft-thread --disable-per-user-config-files $MCA_NO_BUILD --enable-contrib-no-build=libnbc,vt --with-devel-headers --without-valgrind --enable-opal-multi-threads $CONFIG_OTHER

	export NPROCS=${NPROCS:=32}
	make  -j $NPROCS
	make install 
	cd .. 
	rm -r openmpi-$version
	fix_perms -g usg $PREFIX
}

for vsn in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_openmpi gnu $vsn 1.8.4t tcp slurm
done
create-module $PREFIX
