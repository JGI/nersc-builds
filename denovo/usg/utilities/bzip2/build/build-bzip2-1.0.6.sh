#!/bin/bash

set -ex
cd `dirname $0`

vsn=1.0.6
major=`echo $vsn | cut -d\. -f 1`
minor=`echo $vsn | cut -d\. -f 2`
patchlevel=`echo $vsn | cut -d\. -f 3`
short_vsn=${major}.${minor}

file=bzip2-$vsn.tar.gz
url=http://www.bzip.org/$vsn/$file
dir=bzip2-$vsn
[ -f $file ] || wget -O $file $url

function build_it() {
  module purge
  module load PrgEnv-gnu/$env

  PREFIX=$(cd ..; pwd)/gnu${env}/$vsn
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $PREFIX ] && rm -rf $PREFIX
  mkdir -p $PREFIX
  touch $PREFIX/.need-epilogue # Set sentinel for forced rebuilds

  [ -d $dir ] && rm -rf $dir

  tar xf $file
  cd $dir
  export NPROCS=${NPROCS:=32}

  make -j $NPROCS
  make test
  make install PREFIX=$PREFIX

  make clean
  make -j 16 -f Makefile-libbz2_so
  cp libbz2.so.$vsn $PREFIX/lib
  pushd $PREFIX/lib
  ls -l
  ln -s libbz2.so.$vsn libbz2.so.${major}.${minor}
  ln -s libbz2.so.${major}.${minor} libbz2.so.${major}
  ln -s libbz2.so.${major} libbz2.so
  popd
  cd ..
  rm -rf $dir
  fix_perms -g jgitools $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_it
done
create-module $PREFIX
