#!/bin/bash

set -e
prerequisites="openssl/1.1.0c zlib/1.2.11 readline/7.0"
module purge
module load PrgEnv-gnu/7.1
module load perl/5.24.0
module load python/2.7-anaconda
module load gnustuff/1.0
module load $prerequisites

set -ex
cd `dirname $0`

vsn=9.6.2
file=postgresql-$vsn.tar.bz2
url=https://ftp.postgresql.org/pub/source/v$vsn/$file
dir=postgresql-${vsn}
PREFIX=$(cd ..; pwd)/${vsn}
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

mkdir -p $PREFIX
[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget --no-check-certificate -O $file $url
tar xf $file
cd $dir
export CPPFLAGS="$READLINE_INC $GNUSTUFF_INC $ZLIB_INC $OPENSSL_INC"
export LDFLAGS="$READLINE_LIB $GNUSTUFF_LIB $ZLIB_LIB $OPENSSL_LIB"
./configure --prefix=$PREFIX --with-python --with-openssl
export NPROCS=${NPROCS:=32}

make -j $NPROCS world
$EUID || make check # Don't run as root, it spits the dummy
make install-strip
cd ..
rm -rf $dir
fix_perms -g usg $PREFIX
create-module $PREFIX "$prerequisites"
