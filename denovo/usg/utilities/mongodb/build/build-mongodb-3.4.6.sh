#!/bin/bash

set -ex
cd `dirname $0`
module purge
module load PrgEnv-gnu/5.4
module load python/2.7-anaconda
module load gnustuff/1.0
module load scons/2.5.0

export NPROCS=${NPROCS:=32}

vsn=3.4.6
file=mongodb-src-r$vsn.tar.gz
url=http://fastdl.mongodb.org/src/$file
dir=mongodb-src-r$vsn
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir

tar xf $file
cd $dir
patch -p0 <../patch.mongodb.$vsn
#
# Could use 'all', which equates to 'core tools dbtest unittests integration_tests',
# but the other tests are _huge_, taking 10's GB of disk space.
scons --jobs=$NPROCS core tools dbtest CC=`which gcc` CXX=`which g++`

mkdir -p $PREFIX/bin
cp distsrc/{THIRD-PARTY-NOTICES,GNU-AGPL-3.0,LICENSE.OpenSSL,MPL-2,README} $PREFIX
cp mongo{,bridge,d,perf,s} $PREFIX/bin/
chmod -R 775 $PREFIX
strip $PREFIX/bin/mongo*

cd ..
rm -rf $dir $file

fix_perms -g usg $PREFIX

set +ex
echo "*"
echo "*"
echo "* Now run build-mongo-$vsn.tools.sh"
echo "*"
echo "*"
create-module $PREFIX
