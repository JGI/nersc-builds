#!/bin/bash

set -ex
cd `dirname $0`

prerequisites="curl/7.52.1"
module purge
module load PrgEnv-gnu/7.1
module load gnustuff/1.0
# module load zlib/1.2.11    # Pulled in by curl
# module load openssl/1.1.0c # Pulled in by curl
module load golang/1.9.2 # Could use go from gcc
module load $prerequisites

vsn=2.11.0
file=v${vsn}.tar.gz
url=https://github.com/git/git/archive/$file
dir=git-$vsn
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url
tar xf $file

cd $dir
make configure
#
# Can't 'make configure' with a Perl module loaded, there's an error!
module load perl/5.24.0

export CPPFLAGS="$OPENSSL_INC"
export LDFLAGS="$OPENSSL_LIB"
./configure --prefix=$PREFIX \
  --with-curl=$CURL_DIR \
  --with-openssl \
  --with-zlib=$ZLIB_DIR \
  --with-perl=$PERL_DIR/bin/perl
export NPROCS=${NPROCS:=32}

make -j $NPROCS -i # -i skips locale messages, which need msgfmt
make install

cd contrib/subtree
make -j $NPROCS -i
make install
#make install-doc ## don't have asciidoc, so forget this for now
cd ../..

cd ..
rm -rf $dir

lfsvsn=2.3.4
lfsfile=lfs-v${lfsvsn}.tar.gz
lfsdir=git-lfs-${lfsvsn}
lfsurl=https://github.com/git-lfs/git-lfs/archive/v${lfsvsn}.tar.gz
[ -d $lfsdir ] && rm -rf $lfsdir
[ -f $lfsfile ] || wget -O $lfsfile $lfsurl
tar xf $lfsfile
cd $lfsdir
./script/bootstrap
install bin/git-lfs $PREFIX/bin/git-lfs
PATH=$PREFIX/bin:$PATH git lfs install

cd ..
rm -rf $lfsdir

fix_perms -g usg $PREFIX
create-module $PREFIX "$prerequisites"
