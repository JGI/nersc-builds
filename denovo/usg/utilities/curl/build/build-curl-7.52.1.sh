#!/bin/bash

set -ex
cd `dirname $0`

vsn=7.52.1
file=curl-$vsn.tar.gz
url=https://curl.haxx.se/download/$file
dir=curl-$vsn

prerequisites="openssl/1.1.0c zlib/1.2.11"
function build_it() {
  module purge
  module load PrgEnv-gnu/$env
  module load $prerequisites

  [ -f $file ] || wget --no-check-certificate -O $file $url
  [ -d $dir ] && rm -rf $dir
  PREFIX=$(cd ..; pwd)/gnu$env/$vsn
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $PREFIX ] && rm -rf $PREFIX

  tar xf $file
  cd $dir

  export CPPFLAGS="$OPENSSL_INC"
  export LDFLAGS="$OPENSSL_LIB"
  ./configure --prefix=$PREFIX --with-zlib=$ZLIB_DIR

  export NPROCS=${NPROCS:=32}

  make -j $NPROCS
  make install
  cd ..
  rm -r $dir
  fix_perms -g usg $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_it
done
create-module $PREFIX "$prerequisites"
