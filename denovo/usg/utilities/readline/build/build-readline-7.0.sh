#!/bin/sh

set -ex
cd `dirname $0`

function build_it() {
  module purge
  module load PrgEnv-gnu/$env
  module load gnustuff/1.0

  vsn=7.0
  file=readline-$vsn.tar.gz
  dir=readline-$vsn
  url=https://ftp.gnu.org/gnu/readline/$file
  PREFIX=$(cd ..; pwd)/gnu$env/$vsn
  [ -f $file ] || wget -O $file $url
  [ -d $dir ] && rm -rf $dir
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $PREFIX ] && rm -rf $PREFIX

  tar xf $file
  cd $dir

  export CFLAGS="$GNUSTUFF_INC"
  export LDFLAGS="$GNUSTUFF_LIB"
  ./configure --prefix=$PREFIX
  export NPROCS=${NPROCS:=32}

  make -j $NPROCS
  make install
  cd ..
  rm -rf $dir
  fix_perms -g usg $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_it
done
create-module $PREFIX
