#!/bin/bash

prerequisites="qt/4.6.4 gts/0.7.6 gnustuff/1.0 pcre/8.32 libffi/3.2.1 libglib/2.54.1 libjpeg/6b"
module purge
cd `dirname $0`
module load PrgEnv-gnu/4.6
module load python/2.7.4
module load perl/5.18.2
module load $prerequisites

set -ex
vsn=2.38.0

NPROCS=${NPROCS:=32}

PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

export BASE_DEPENDENCIES_CFLAGS="-I$PREFIX/include"
export BASE_DEPENDENCIES_LIBS="-L$PREFIX/lib -L$PREFIX/lib64"
export LIBFFI_LIBS="$LIBFFI_LIB -lffi"
export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig

#
# gdk-pixbuf
pbvsn_short=2.36
pbvsn=${pbvsn_short}.10
pbfile=gdk-pixbuf-$pbvsn.tar.xz
pbdir=gdk-pixbuf-$pbvsn
pburl=https://ftp.gnome.org/pub/gnome/sources/gdk-pixbuf/$pbvsn_short/$pbfile
[ -d $pbdir ] && rm -rf $pbdir
[ -f $pbfile ] || wget -O $pbfile $pburl
tar xf $pbfile
cd $pbdir

export CPPFLAGS="-I$PREFIX/include $LIBGLIB_INC $JPEG_INC"
export LDFLAGS="$JPEG_LIB -ljpeg"
export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig
export LD_LIBRARY_PATH=$PREFIX/lib:$LD_LIBRARY_PATH

./configure --prefix=$PREFIX \
    --without-libtiff
make -j $NPROCS
make install
cd ..
rm -rf $pbdir

file=graphviz-$vsn.tar.gz
dir=graphviz-$vsn
url=http://www.graphviz.org/pub/graphviz/stable/SOURCES/$file
[ -d $dir ] && rm -rf $fdir
[ -f $file ] || wget -O $file $url

tar xf $file
cd $dir
./configure --prefix=$PREFIX \
  --enable-static=yes \
  --enable-shared=no \
  --enable-tcl=no

make -j $NPROCS
make install
cd ..
rm -rf $dir

fix_perms -g usg $PREFIX
create-module $PREFIX "$prerequisites"
