#!/bin/bash

prerequisites="boost/1.59.0"
module purge
module load PrgEnv-gnu/4.9
module load cmake/3.9.0
module load gnustuff/1.0
module load git/2.11.0
module load $prerequisites

set -ex
cd `dirname $0`

vsn=5.6.36
file=mysql-boost-${vsn}.tar.gz
file=mysql-${vsn}.tar.gz
dir=mysql-${vsn}
PREFIX=$(cd ..; pwd)/${vsn}

[ -d $dir ] && rm -rf $dir
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
if [ ! -f $file ]; then
  echo "You need to sign up for an account at mysql.com to download the source"
  echo "for version $vsn. Save it here as $file and try again."
  exit 0
fi

tar xf $file

cd $dir
mkdir build
cd build
cmake ../ -Wno-dev \
  -DCMAKE_INSTALL_PREFIX=$PREFIX \
  -DLOCAL_BOOST_DIR=${BOOST_DIR}/include \
  -DENABLED_LOCAL_INFILE=TRUE \
  -DCURSES_LIBRARY=$GNUSTUFF_DIR/lib/libncurses.so \
  -DCURSES_INCLUDE_PATH=$GNUSTUFF_DIR/include

export NPROCS=${NPROCS:=32}

make -j $NPROCS
make test
make install
strip $PREFIX/bin/* || true # ignore un-strippable files
cd ../..
rm -r $dir
fix_perms -g usg $PREFIX
create-module $PREFIX "$prerequisites"
