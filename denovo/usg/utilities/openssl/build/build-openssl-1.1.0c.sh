#!/bin/bash

set -ex
cd `dirname $0`

vsn=1.1.0c
file=openssl-$vsn.tar.gz
dir=openssl-$vsn
url=https://www.openssl.org/source/$file

function build_it() {
  module purge
  module load PrgEnv-gnu/$env
  module load perl/5.24.0

  PREFIX=$(cd ..; pwd)/gnu$env/$vsn

  [ -f $file ] || wget --no-check-certificate -O $file $url
  [ -d $dir ] && rm -rf $dir
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $PREFIX ] && rm -rf $PREFIX
  tar xf $file

  cd $dir
# add 'no-shared' following INC0100456, except I don't want to do that
  ./config --prefix=$PREFIX # no-shared

  export NPROCS=${NPROCS:=32}

  make -j $NPROCS
  make test
  make install
  cd ..
  rm -rf $dir
  fix_perms -g usg $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_it
done
create-module $PREFIX
