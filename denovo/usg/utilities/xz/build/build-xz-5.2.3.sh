#!/bin/bash

set -ex
cd `dirname $0`

vsn=5.2.3
file=xz-$vsn.tar.gz
url=http://tukaani.org/xz/xz-$vsn.tar.gz
dir=xz-$vsn

function build_it() {
  module purge
  module load PrgEnv-gnu/$env
  module load gnustuff/1.0
  [ -f $file ] || wget -O $file $url
  [ -d $dir ] && rm -rf $dir
  PREFIX=$(cd ..; pwd)/gnu$env/$vsn
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $PREFIX ] && rm -rf $PREFIX

  tar zxf $file
  cd $dir

# flags other than --prefix following INC0100456, except I don't like disabling shared libs...
  ./configure --prefix=$PREFIX CFLAGS=-fPIC --enable-static # --disable-shared

  export NPROCS=${NPROCS:=32}

  make -j $NPROCS
  make check
  make install
  cd ..
  rm -r $dir
  fix_perms -g usg $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_it
done
create-module $PREFIX
