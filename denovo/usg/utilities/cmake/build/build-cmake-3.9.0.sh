#!/bin/bash

set -ex
cd `dirname $0`

vshort=3.9
vsn=${vshort}.0
dir=cmake-$vsn
file=cmake-$vsn.tar.gz
url=https://cmake.org/files/v$vshort/$file

function build_it() {
  module purge
  module load PrgEnv-gnu/$env
  PREFIX=$(cd ..; pwd)/gnu${env}/$vsn
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $PREFIX ] && rm -rf $PREFIX
  [ -d $dir ] && rm -rf $dir
  [ -f $file ] || wget -O $file $url

  tar xf $file
  cd $dir

  ./bootstrap --prefix=$PREFIX

  export NPROCS=${NPROCS:=32}

  make -j $NPROCS
  make install
  cd ..
  rm -r $dir
  fix_perms -g usg $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_it
done
create-module $PREFIX
