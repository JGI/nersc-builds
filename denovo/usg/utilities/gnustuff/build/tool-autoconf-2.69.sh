#!/bin/bash

set -ex
cd `dirname $0`

. ./env.sh

vsn=2.69
tool=autoconf
file=${tool}-${vsn}.tar.gz
url=https://ftp.gnu.org/gnu/${tool}/$file
dir=${tool}-${vsn}

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url
tar xf $file
cd $dir
./configure --prefix=$PREFIX
make -j $NPROCS
make install
cd ..
rm -rf $dir
