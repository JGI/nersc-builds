#!/bin/bash

set -ex
cd `dirname $0`

. ./env.sh

module load gnustuff/1.0

vsn=7.3.5
tool=dos2unix
file=${tool}-${vsn}.tar.gz
url=https://downloads.sourceforge.net/project/${tool}/${tool}/${vsn}/$file
dir=${tool}-${vsn}

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url
tar xf $file
cd $dir
make -j $NPROCS
# make check # don't have 'prove'
make strip
make install DESTDIR=$PREFIX prefix=/
cd ..
rm -rf $dir
