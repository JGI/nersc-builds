#!/bin/bash

set -ex
cd `dirname $0`

. ./env.sh

vsn=6.0
tool=ncurses
file=${tool}-${vsn}.tar.gz
url=https://ftp.gnu.org/gnu/${tool}/$file
dir=${tool}-${vsn}

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url
tar xf $file
cd $dir
CPPFLAGS=-P \
  ./configure --prefix=$PREFIX --with-shared
make -j $NPROCS
make install
cd ..
rm -rf $dir
