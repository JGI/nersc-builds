#!/bin/bash

set -ex
cd `dirname $0`

. ./env.sh
module load zlib/1.2.11

vsn=2.3.4
tool=pigz
file=${tool}-${vsn}.tar.gz
url=https://zlib.net/$tool/$file
dir=${tool}-${vsn}

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url
tar xf $file
cd $dir
export CFLAGS="-O3 -Wall -Wextra $ZLIB_INC"
export LDFLAGS="$ZLIB_LIB"
make -j $NPROCS CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" CC=gcc
make tests      CFLAGS="$CFLAGS" LDFLAGS="$LDFLAGS" CC=gcc
cp pigz $PREFIX/bin
cd ..
rm -rf $dir
cd $PREFIX/bin
chmod o+rx pigz
ln -f pigz unpigz
