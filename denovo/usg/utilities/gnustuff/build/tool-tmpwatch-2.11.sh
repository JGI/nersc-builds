#!/bin/bash

set -ex
cd `dirname $0`

. ./env.sh

vsn=2.11
tool=tmpwatch
file=${tool}-${vsn}.tar.bz2
#
# Oh yes, this is obsolete!
url=https://web.archive.org/web/20150802065841if_/https://fedorahosted.org/releases/t/m/${tool}/$file
dir=${tool}-${vsn}

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url
tar xf $file
cd $dir
./configure --prefix=$PREFIX
make -j $NPROCS
make install
cd ..
rm -rf $dir
