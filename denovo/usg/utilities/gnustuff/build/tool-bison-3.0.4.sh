#!/bin/bash

set -ex
cd `dirname $0`

. ./env.sh

vsn=3.0.4
tool=bison
file=${tool}-${vsn}.tar.gz
url=http://ftp.wayne.edu/gnu/${tool}/$file
dir=${tool}-${vsn}

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url
tar xf $file
cd $dir
./configure --prefix=$PREFIX
make -j $NPROCS
make install
cd ..
rm -rf $dir
