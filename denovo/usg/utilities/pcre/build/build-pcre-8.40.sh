#!/bin/bash

set -ex
cd `dirname $0`

function build_it() {
  module purge
  module load PrgEnv-gnu/$env

  vsn=8.40
  file=pcre-$vsn.tar.bz2
  dir=pcre-$vsn
  url=https://ftp.pcre.org/pub/pcre/$file
  PREFIX=$(cd ..; pwd)/gnu$env/$vsn
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $PREFIX ] && rm -rf $PREFIX
  [ -f $file ] || wget --no-check-certificate -O $file $url
  [ -d $dir ] && rm -rf $dir

  tar xjf $file
  cd $dir
  ./configure --prefix=$PREFIX --enable-utf CFLAGS=$GCC_RECORD_SWITCHES
  export NPROCS=${NPROCS:=32}

  make -j $NPROCS
  make test
  make install
  cd ..
  rm -r $dir
  fix_perms -g usg $PREFIX/..
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_it
done
create-module $PREFIX
