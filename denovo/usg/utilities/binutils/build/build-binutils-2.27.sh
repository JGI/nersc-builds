#!/bin/bash

set -ex
cd `dirname $0`

export NPROCS=${NPROCS:=32}

function build_library {
	prgenv=$1
	prgenv_version=$2
	version=$3
	module purge
	module load PrgEnv-$prgenv/$prgenv_version
	ORIGDIR=`pwd`
	PREFIX=$(cd ..; pwd)/$prgenv$prgenv_version/$version
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
	[ -d $PREFIX ] && rm -rf $PREFIX

	dir=binutils-$version
	file=binutils-$version.tar.gz
	if [ -e $dir ]; then
		rm -rf $dir
	fi
	[ -f $file ] || \
	  wget --no-check-certificate \
	    -O $file \
	    https://ftp.gnu.org/gnu/binutils/$file
	tar xf $file
	cd $dir
	./configure --prefix=$PREFIX LDFLAGS="--static"
 	make -j $NPROCS
 	make install-strip
 	cd ..
	rm -rf binutils-$version
	fix_perms -g usg $PREFIX
}

vsn=2.27
for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_library gnu $env $vsn
done
create-module $PREFIX
