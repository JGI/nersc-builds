#!/bin/bash

set -e
cd `dirname $0`

function build_library {
	prgenv=$1
	prgenv_version=$2
	version=$3
	module purge
	module load PrgEnv-$prgenv/$prgenv_version
	ORIGDIR=`pwd`
	PREFIX=$(cd ..; pwd)/$prgenv$prgenv_version/$version
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $PREFIX ] && rm -rf $PREFIX

  [ -d $dir ] && rm -rf $dir
	tar xf $file
	cd $dir
	./configure --prefix=$PREFIX
	make -j $NPROCS
 	make install
 	cd ..
	rm -rf $dir
	fix_perms -g usg $PREFIX
}

vsn=1.2.11
file=zlib-$vsn.tar.gz
url=http://www.zlib.net/$file
dir=zlib-$vsn
[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir

export NPROCS=${NPROCS:=32}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_library gnu $env $vsn
done
create-module $PREFIX
