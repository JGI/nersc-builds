#!/bin/sh

set -ex
cd `dirname $0`

function build_it() {
  module purge
  module load PrgEnv-gnu/$env

  vsn=2.7.1
  file=freetype-$vsn.tar.gz
  url=http://download.savannah.gnu.org/releases/freetype/$file
  dir=freetype-$vsn
  PREFIX=$(cd ..; pwd)/gnu$env/$vsn

  [ -f $file ] || wget --no-check-certificate -O $file $url
  [ -d $dir ] && rm -rf $dir
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $PREFIX ] && rm -rf $PREFIX
  tar xf $file
  cd $dir

  ./configure --prefix=$PREFIX
  export NPROCS=${NPROCS:=32}

  make -j $NPROCS
  make install

  cd ..
  rm -rf $dir
  fix_perms -g usg $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_it
done
create-module $PREFIX
