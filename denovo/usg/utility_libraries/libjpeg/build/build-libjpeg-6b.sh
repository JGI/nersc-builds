#!/bin/bash

export NPROCS=${NPROCS:=32}
cd `dirname $0`

function build_it() {
  module purge
  module load PrgEnv-gnu/$env
  module load gnustuff/1.0

  set -e
  vsn=6b
  PREFIX=$(cd ..; pwd)/gnu$env/$vsn
  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $PREFIX ] && rm -rf $PREFIX

  file=jpegsr6.zip
  url=https://downloads.sourceforge.net/project/libjpeg/libjpeg/$vsn/$file
  dir=jpeg-$vsn
  [ -f $file ] || wget --no-check-certificate -O $file $url
  [ -d $dir ] && rm -rf $dir
  unzip $file
  cd $dir
#
# Need to apply dos2unix to configure, ltconfig, ltmain.sh, config.sub and config.guess too.
# Doing so breaks things, since ltconfig then gets system type 'x86_64-unknown-linux-gnu',
# and it doesn't know about 'x86*'. So, try adding --host=i686-linux-gnu to the configure
# arguments...
#
# ...but that doesn't work either, for some reason. So, hack config.guess instead, to force
# it to return that string
#
#
  for file in configure ltconfig ltmain.sh config.sub config.guess
  do
    dos2unix $file
  done

  (
    cat <<-EOF
	#!/bin/bash
	echo i686-linux-gnu
	exit 0
EOF
  ) | tee config.guess >/dev/null
  chmod +x config.guess

  LIBTOOL=`which libtool` ./configure --prefix=$PREFIX --enable-shared # --host=i686-linux-gnu # Doesn't work, as noted above...
  make -j $NPROCS
# Fix jconfig.h, in case it's broken... This shows up in later tools; meraculous, or gnuplot, IIRC?
  mv jconfig.h jconfig.h.bak
  cat jconfig.h.bak | \
    sed -e 's%^.*HAVE_PROTOTYPES.*$%#define HAVE_PROTOTYPES # TW: Force by hand%' | \
    tee jconfig.h

  mkdir -p $PREFIX/{man/man1,include,lib,bin}
  make install
  cp jerror.h $PREFIX/include/
  cd ..
  rm -rf $dir

  fix_perms -g usg $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_it
done
create-module $PREFIX
