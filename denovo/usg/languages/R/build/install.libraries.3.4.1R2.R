options(repos="http://cran.cnr.Berkeley.edu")

#
# libraries specified by M. Dunford
#

install.packages("HMP")
install.packages("HMPTrees")
install.packages("phangorn")
install.packages("RSPerl")
install.packages("pbh5")
install.packages("h5r")
install.packages("gbm")
install.packages("gdata")
install.packages("gtools")
install.packages("gmodels")
install.packages("gplots")
install.packages("Rmpi")
install.packages("MEMSS")
install.packages("VGAM")
install.packages("ggplot2")
install.packages("nws")
install.packages("plyr")
install.packages("pvclust")
install.packages("reshape")
install.packages("rlecuyer")

# bioconductor libraries, which include various other packages.
options(BioC_mirror="http://www.bioconductor.org")
source("http://bioconductor.org/biocLite.R")

biocLite("Biobase")
biocLite(c('IRanges', 'AnnotationDbi'))
biocLite("ShortRead")
biocLite("affyPLM")
biocLite("affyQCReport")
biocLite("limma")
biocLite("marray")
biocLite("vsn")

#
# installed for Abhi Pratap
#
biocLite("DESeq")
biocLite("DEXSeq")
biocLite("RBGL")
biocLite("ggbio")
biocLite("Rsamtools")
# Rsamtools previously needed, but OK for 2.15.0 as above (  PKG_CFLAGS=-Dinline="" R CMD INSTALL --clean  Rsamtools_1.6.3.tar.gz)

#
# installed for Guohon Wu
#
install.packages("Rmpfr")

#
# installed for Xiandong Meng
# 
biocLite("RankProd")


#
# installed for Sirisha Sunkara
#
# Bioconductor:
#biocLite("IRanges")
#biocLite("GenomicRanges")
#biocLite("genomeIntervals")
biocLite("Biostrings")
biocLite("HilbertVis")
biocLite("HilbertVisGUI")
biocLite("Genominator")
biocLite("LBE")
biocLite("bioDist")
biocLite("cnvGSA")
biocLite("CNVtools")
biocLite("mops")

## DMJ - Installed for Kristin Tennessen SN INC0020585
biocLite("phyloseq")

# dupes
#install.packages("ggplot2
#install.packages("RSQLite
#install.packages("plyr
#install.packages("lattice


#
#  installed by ddkang for 2.14.1 -- check if all needed?
#

#"BiocInstaller"
#"Biobase"
#"AnnotationDbi"
#"geneplotter"
#"annotate"
#"genefilter"
#"Biostrings"
#"IRanges"
##install.packages("DBI")
#install.packages("RSQLite")
#install.packages("Matrix")
#install.packages("boot")
#install.packages("nlme")
#install.packages("rpart")
#install.packages("mgcv")
#install.packages("proto")
#install.packages("RColorBrewer")
#install.packages("digest")
#install.packages("colorspace")
#install.packages("iterators")
#install.packages("multicore")
#install.packages("foreach")
#install.packages("doMC")
#install.packages("dynamicTreeCut")
#install.packages("flashClust")
#install.packages("WGCNA")
#install.packages("akima")
#install.packages("locfit")
#install.packages("impute")
#install.packages("bitops")
#install.packages("caTools")
#install.packages("amap")
#install.packages("proxy")
#install.packages("cba")
#install.packages("ctc")
#install.packages("matrixStats")
#install.packages("Rcpp")
#install.packages("R.methodsS3")
#install.packages("R.oo")
#install.packages("R.utils")
#install.packages("lars")
#install.packages("lasso2")
#install.packages("partykit")
#install.packages("svmpath")
#install.packages("ROCR")
#install.packages("randomForest")
#install.packages("skmeans")
#install.packages("e1071")
#install.packages("kernlab")
#install.packages("corrplot")
#install.packages("pls")
#install.packages("pcaMethods")
#install.packages("mlbench")
#install.packages("snow")
#install.packages("fastICA")
#install.packages("neuralnet")
#install.packages("superpc")
#install.packages("getopt")
#install.packages("md5")
#install.packages("pROC")
#install.packages("MetaQC")
#install.packages("mclust")
#install.packages("mvtnorm")
#install.packages("modeltools")
#install.packages("multcomp")
#install.packages("flexmix")
#install.packages("fpc")
#install.packages("lpSolve")
#install.packages("irr")
#install.packages("compare")
#install.packages("leaps")
#install.packages("doMPI")
#install.packages("MASS")
#install.packages("cluster")
#install.packages("foreign")
#install.packages("graph")
#install.packages("survival")
#install.packages("xtable")
#install.packages("Hmisc")
#install.packages("caret")
#install.packages("optparse")
#install.packages("preprocessCore")
#install.packages("aroma.light")
#install.packages("elasticnet")
#install.packages("sparcl")
#install.packages("latticeExtra")
#install.packages("clv")
#install.packages("siatclust")
#install.packages("slam")
#install.packages("clue")
#install.packages("apcluster")
#install.packages("LiblineaR")
#install.packages("seqinr")
#install.packages("CHNOSZ")
#install.packages("bigmemory")
#install.packages("biganalytics")

install.packages("doParallel")

## found these comparing the jgitools R 2.15.0 to this 2.15.1 directory; 20120724 DMJ
biocLite("baySeq")
biocLite("edgeR")
install.packages("genoPlotR")
install.packages("getopt")
install.packages("lubridate")
install.packages("optparse")

install.packages(c("RColorBrewer", "gplots", "latticeExtra"))
biocLite("aroma.light")
biocLite("ctc")

install.packages("phytools")
install.packages("packrat")
