#!/bin/bash

set -ex
cd `dirname $0`

if [ "$NERSC_BUILD_ALLOW_ANY_USER" != "true" ]; then
  if [ "$USER" != "gpinterp" ]; then
    echo "Please install this s/w via the gpinterp account"
    exit 0
  fi
fi

# Need libpixman-1. Zypper installs to /usr/lib64 with '.0' extension
# Need libcairo2. Zypper installs to /usr/lib64 with '.2' extension
# Need libiconv. Zypper doesn't find one...
# Use https://ftp.gnu.org/pub/gnu/libiconv/libiconv-1.15.tar.gz

export LANG=en_US
export LANGUAGE=en_US:en

prerequisites="readline/7.0 xz/5.2.3 curl/7.52.1 libpng/1.6.28 libjpeg/6b pcre/8.40 bzip2/1.0.6"
module purge
module load PrgEnv-gnu/7.1
module load java/1.8_64bit
module load gnustuff/1.0
module load $prerequisites

vsn=3.3.3
file=MRO-$vsn.tar.gz
url=https://github.com/Microsoft/microsoft-r-open/archive/$file
dir=microsoft-r-open-MRO-$vsn
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0

#
# Sanity-check: make sure I _cannot_ write R modules into my home directory
if [ -d ~/R ]; then
  echo "~/R exists and is a directory, aborting to prevent risk of bad installation"
  exit 1
fi
if [ ! -e ~/R ]; then
  echo "Blocking possibility of installing into ~/R/"
  ln -s /dev/null ~/R
fi

export NPROCS=${NPROCS:=32}
[ -d $PREFIX ] && rm -rf $PREFIX
[ -f $file ] || wget --no-check-certificate -O $file $url
[ -d $dir ] && ( echo "Cleaning up: R" && rm -rf $dir )
echo Unpacking $file
tar xf $file
cd $dir/source
./tools/rsync-recommended

export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$PREFIX/lib
export CXXFLAGS="$GCC_RECORD_SWITCHES -O2"
export FFLAGS="-O2"
export CFLAGS="$GCC_RECORD_SWITCHES -O2"
export CPPFLAGS="$READLINE_INC $XZ_INC $CURL_INC $LIBPNG_INC $LIBJPEG_INC $PCRE_INC $GNUSTUFF_INC $OPENSSL_INC $ZLIB_INC $BZIP2_INC -I${PREFIX}/include"
export LDFLAGS="$GCC_RECORD_SWITCHES -O2 $READLINE_LIB $XZ_LIB $CURL_LIB $LIBPNG_LIB $LIBJPEG_LIB $PCRE_LIB $GNUSTUFF_LIB $OPENSSL_LIB $ZLIB_LIB $BZIP2_LIB -L${PREFIX}/lib"
export LIBS="-lreadline -lncurses -llzma -lcurl -lpng -lcairo -lpixman-1 -ljpeg -lz -lbz2"
# Try pruning the list: TW
export LIBS="-lreadline -lncurses -llzma -lcurl -lpng -ljpeg -lz -lbz2"

#
# This shouldn't be necessary now: TW
# export PKG_CONFIG_PATH=${PKG_CONFIG_PATH}:${LIBPNG_DIR}/lib/pkgconfig:$PCRE_DIR/lib/pkgconfig:${PREFIX}/lib/pkgconfig

#
# Have some patching to do...
#
# ZLIB versions with more than 5 characters (e.g. '1.2.11') fail the 5-char test
for f in {configure,m4/R.m4}
do
  echo Fix $f for ZLIB_VERSION bug
  mv $f $f.sav
  cat $f.sav | \
    sed -e 's%strncmp(ZLIB_VERSION, "1.2.5", 5) < %%' | \
    tee $f >/dev/null
done
chmod +x configure

./configure --prefix=$PREFIX \
  --with-cairo \
  --with-libpng \
  --with-jpeglib \
  --with-readline \
  --enable-R-shlib

make -j $NPROCS
make check
make install
cd ../..
rm -rf $dir

# $PREFIX/bin/R CMD BATCH install.libraries.$vsn.R

# Can't just use $USER because I may be fudging the environment to allow
# building everything on Denovo in one go
REALUSER=`id --user --name`
fix_perms -g $REALUSER $PREFIX
fix_perms -g $REALUSER .
create-module $PREFIX "$prerequisites"
