#!/bin/bash

set -ex
cd `dirname $0`

vsn=1.8.0_31_x86_64
file=jdk-8u31-linux-x64.tar.xz

PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0

[ -f $file ] || (
  echo "No source file: $file"
  exit 1
)

[ -d $PREFIX ] & rm -rf $PREFIX
mkdir -p $PREFIX
tar xf $file --strip-components=1 -C $PREFIX
fix_perms -g usg $PREFIX
create-module $PREFIX
