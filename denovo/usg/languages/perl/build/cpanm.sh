#!/bin/sh

set -e

if [ "X$1" != "X" ]; then
  MODULES=$1
else
  if [ -f modules.$vsn.list ]; then
    MODULES=modules.$vsn.list
  else
    MODULES=modules.list
  fi
fi
echo Using MODULES=$MODULES
INSTALLED=modules.installed.$vsn.list
[ -f $INSTALLED ] && rm -f $INSTALLED

if [ ! -x cpanm ]; then
  wget --no-check-certificate -O cpanm https://cpanmin.us/
  chmod +x cpanm
fi
if [ ! -f $PREFIX/bin/cpanm ]; then
  cp cpanm  $PREFIX/bin/cpanm
  chmod 755 $PREFIX/bin/cpanm
fi

[ -d ~/.cpanm ] && rm -rf ~/.cpanm
echo "Installing modules in $PERL_LOCAL_LIB_ROOT"

#
# Net::SSLeay needs help finding zlib. Beaurk!
if [ `grep -c Net::SSLeay $MODULES` -gt 0 ]; then
  export OPENSSL_PREFIX=$OPENSSL_DIR
  module=Net::SSLeay
  ./cpanm --installdeps -l $PERL_LOCAL_LIB_ROOT $module
  nsvsn=1.81
  nsfile=Net-SSLeay-${nsvsn}.tar.gz
  nsdir=Net-SSLeay-${nsvsn}
  nsurl=https://cpan.metacpan.org/authors/id/M/MI/MIKEM/$nsfile
  [ -f $nsfile ] || wget -O $nsfile $nsurl
  [ -d $nsdir ] && rm -rf $nsdir
  tar xf $nsfile
  cd $nsdir
  perl Makefile.PL PREFIX=$PERL_LOCAL_LIB_ROOT
  make -j $NPROCS LDLOADLIBS="-L${OPENSSL_DIR}/lib -lssl -lcrypto -L${ZLIB_DIR}/lib -lz"
  make test
  make install
  cd ..
  rm -rf $nsdir
fi

for module in `cat $MODULES | grep -v '#'`; do
  echo " "
  echo "Install dependencies for $module"
  #
  # Would like to parallelize the dependency installations, but that doesn't
  # work properly. Seems to be some sort of race condition?
  # ./cpanm -l $PERL_LOCAL_LIB_ROOT --showdeps --quiet CGI | \
  #   xargs --max-args 1 --max-procs $NPROCS ./cpanm -l $PERL_LOCAL_LIB_ROOT
  ./cpanm --installdeps -l $PERL_LOCAL_LIB_ROOT $module
  echo "..."
  echo "Install $module"
  ./cpanm -l $PERL_LOCAL_LIB_ROOT $module
  ./cpanm --info $module | egrep -v '^!' | tee -a $INSTALLED
  echo " "
done

#
# DBD::Oracle requires checking if ORACLE_HOME is set & exists
if [ -n "$ORACLE_HOME" ]; then
  if [ -d $ORACLE_HOME ]; then
    module=DBD::Oracle
    ./cpanm --installdeps -l $PERL_LOCAL_LIB_ROOT $module
    ./cpanm -l $PERL_LOCAL_LIB_ROOT $module
    ./cpanm --info $module | egrep -v '^!' | tee -a $INSTALLED
  else
    echo "ORACLE_HOME is set ($ORACLE_HOME), but is not a directory!"
    exit 1
  fi
else
  echo "Skipping DBD::Oracle because ORACLE_HOME is not set. No oracle_client module?"
fi
