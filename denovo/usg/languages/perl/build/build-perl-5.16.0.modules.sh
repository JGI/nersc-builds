#!/bin/bash

set -ex
export vsn=5.16.0
cd `dirname $0`
. ./env.sh
[ -d $PERL_LOCAL_LIB_ROOT ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
#
# These modules are needed for the DBD::xyz Perl modules, not for the main build
module load mysql/5.7.19
module load postgresql/9.6.2
module load openssl/1.1.0c
module load zlib/1.2.11

export NPROCS=${NPROCS:=32}

./cpanm.sh

echo " "
echo "Easy modules installed, now for GD!"
echo " "

./GD.sh

echo " "
echo "And now for XML::LibXML2"
echo " "

./XML-LibXML2.sh

echo " "
echo "And EnvironmentModules.pm..."
echo " "
cp EnvironmentModules.pm $PERL_LOCAL_LIB_ROOT/lib/perl5/

echo " "
echo "Record the full set of installed modules"
echo " "

cpan -l | tee MANIFEST.$vsn

echo " "
echo "All done!"
echo " "

# Can't just use $USER because I may be fudging the environment to allow
# building everything on Denovo in one go
REALUSER=`id --user --name`
fix_perms -g $REALUSER $PREFIX
fix_perms -g $REALUSER .
