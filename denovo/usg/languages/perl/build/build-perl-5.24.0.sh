#!/bin/bash

set -ex
export vsn=5.24.0
cd `dirname $0`
. ./env.sh

[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
echo "Installing into $PREFIX"
[ -d $PREFIX ] && rm -rf $PREFIX

file=perl-$vsn.tar.gz
url=http://www.cpan.org/src/5.0/$file
dir=perl-$vsn
[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir

tar zxf $file
cd $dir

./Configure \
  -Dusethreads \
  -Duselargefiles \
  -Duse64bitall \
  -Doptimize=-O3 \
  -Dpager=/usr/bin/less \
  -Dprefix=$PREFIX \
  -Dcc=gcc \
  -de

export NPROCS=${NPROCS:=32}

make -j $NPROCS

unset PERL5LIB
make test
make install

cd ..
rm -rf $dir

echo " "
echo "PERL installed, yay!"
echo "Record the full set of installed modules"
echo " "

cpan -l | tee MANIFEST.$vsn

echo " "
echo "All done!"
echo " "

# Can't just use $USER because I may be fudging the environment to allow
# building everything on Denovo in one go
REALUSER=`id --user --name`
fix_perms -g $REALUSER $PREFIX
fix_perms -g $REALUSER .
create-module $PREFIX
