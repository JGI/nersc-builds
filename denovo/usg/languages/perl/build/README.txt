Here's the recipe to build Perl.

1) make sure you're running as 'gpinterp', not a normal user. If you build as
yourself, and then attempt to install Perl modules in your home directory, you
may end up installing in the system directory instead, and that can mess up
filesystem permissions.

2) build Perl. This includes testing it, which takes some time. It's not
uncommon for a test to fail, you need to investigate why, fix it if you can,
or work round it.

> ./build-perl-$vsn.sh

The build script will attempt to download Perl if it isn't there, so in principle
this is all you'll need to do.

Next, you need to build all the extra Perl modules we provide. This varies
by Perl-version, some modules are no longer supported, or are integrated into
the core etc. Each Perl version has its specific module-build script,
"build-perl-$vsn.modules.sh".

The one caveat with building modules is that many of them depend on other tools,
such as PostGRES, MySQL or Oracle. Some of them, in turn, depend on Perl to build.
So if you're building on a clean platform, you'll have to build the base Perl
first, then the other tools that use it, then come back and build the modules.

The module-build script will look for a "modules.$vsn.list" file, or if that
doesn't exist, it'll use "modules.list" instead. If you're building a new version
of Perl, take the closest lower version of modules*list as your base reference.

The extra modules are taken from CPAN, and the latest version is used at the time
of the build. This isn't always reproducible, sometimes the latest version has
bugs, so you may have to specify a specific version. Check the 'cpanm'
documentation for how to do that.

The actual module versions installed are recorded in "modules.installed.$vsn.list"
files, for each version. If you need to re-create that precise version of Perl,
you can do something based on that. I don't provide a script that does that.

Note that the modules each have their own dependencies, and we don't fully control
the versions of the dependencies that are installed. To see the full list of
installed modules, see the 'MANIFEST.$vsn' files, produced by 'cpan -l' at the
end of the build.

#------------------------------------------------------------

If you need to add a new module by hand, after the main installation, here's how.

The easy way:
- add the module name to the appropriate module*.list file

- run ./build-perl-$vsn.modules.sh

If you prefer the hard way, just manually adding a single module, you can do that too,
but of course you may lose reproducibility that way:

- set $vsn in your environment

- source the env.sh script

- clean out the cpan build area, if it exists
  > rm -rf ~/.cpanm-$vsn-`hostname`

- install the dependencies, then the module itself
  > ./cpanm --installdeps -l $PERL_LOCAL_LIB_ROOT your_module
  > ./cpanm -l $PERL_LOCAL_LIB_ROOT your_module

- record the version you installed
  >  ./cpanm --info $module | egrep -v '^!' | tee -a modules.installed.$vsn.list
  > cpan -l | tee MANIFEST.$vsn

- fix the filesystem permissions, so users can see it:
  > fix_perms -g gpinterp ../$vsn/extra
  > # also, in the perl/build directory, so users can read the MANIFEST etc
  > chmod o+r *
