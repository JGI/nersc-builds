#!/bin/bash

set -ex
cd `dirname $0`
module purge

vsn=1.4.2
file=go${vsn}.linux-amd64.tar.gz
url=https://redirector.gvt1.com/edgedl/go/go1.4.2.linux-amd64.tar.gz
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
mkdir -p $PREFIX
cd $PREFIX

[ -f $file ] || wget -O $file $url
tar xf $file
rm $file
mv go/* .
rm -rf go

fix_perms -g usg $PREFIX
create-module $PREFIX
