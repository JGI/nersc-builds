#!/bin/bash

set -ex
cd `dirname $0`

if [ "$NERSC_BUILD_ALLOW_ANY_USER" != "true" ]; then
  if [ "$USER" != "gpinterp" ]; then
    echo "Please install this s/w via the gpinterp account"
    exit 1
  fi
fi

vsn=2.7
PREFIX=$(cd ..; pwd)/${vsn}-anaconda
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
file=Anaconda2-4.4.0-Linux-x86_64.sh
url=https://repo.continuum.io/archive/$file
[ -f $file ] || wget -O $file $url
[ -d $PREFIX ] && rm -rf $PREFIX

chmod +x $file
./$file -b -p $PREFIX

export PATH=${PREFIX}/bin:$PATH
export LD_LIBRARY_PATH=${PREFIX}/bin:$LD_LIBRARY_PATH
pip --no-cache install --upgrade pip
pip --no-cache install --requirement packages.txt
pip freeze | tee MANIFEST.$vsn.txt
 
# Can't just use $USER because I may be fudging the environment to allow
# building everything on Denovo in one go
REALUSER=`id --user --name`
fix_perms -g $REALUSER $PREFIX
fix_perms -g $REALUSER .

cd $PREFIX/lib
mkdir lib.sav
mv libtcl* libstdc++* libreadline.* libgomp.* lib.sav
create-module $PREFIX
