Anaconda can be downloaded from https://repo.continuum.io/archive/

These python versions come with their own Tcl libraries, in $ROOT/lib/libtcl*.
Those libraries break the 'module command', so I created a lib/lib.sav/
directory and moved them there.

Hopefully nobody at JGI is actually using Tcl with python, so it shouldn't matter.

Also, these distributions come with their own libstdc++.so.6, and because the module
prepends LD_LIBRARY_PATH, that library takes precedence over the one provided by the
compiler. So rename the libstdc++.so* libraries into lib/lib.sav/ too.

...and libgomp*...

The 'module' command sucks...
