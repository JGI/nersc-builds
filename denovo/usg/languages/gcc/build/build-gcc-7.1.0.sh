#!/bin/sh

cd `dirname $0`
export gcc_vsn=7.1.0
export binutils_vsn=2.28
export build_with=`( ls -1 ..; echo $gcc_vsn ) | sort --version-sort | awk "/$gcc_vsn/{exit};1" | tail -1`
export extra_languages=,go

./gcc-generic-build.sh
