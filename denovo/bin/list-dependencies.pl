#!/usr/bin/env perl
use strict;
use warnings;
use File::Basename;

my ( $file, %h, $script, $path, $package, %seen, $dependencies, %need, @args );

scalar @ARGV or die "Need a list of packages to examine\n";

chdir( dirname $0 ) || die "Cannot chdir: $!\n";
foreach $file ( glob( "../{usg,jgi}/*/*/build/*.sh" ) ) {
  # print $file, "\n";
  $file =~ m%\.\./(([^/]+)/[^/]+/([^/]+)/build/)(.*)$%;
  $path = $1;
  $package = $3;
  $script = $path . $4;
  next if $package eq 'bupc';
  defined( $h{ $package } ) or $h{ $package } = {};

  open SCRIPT, "<../$script" or die "Open $script: $!\n";
  while ( <SCRIPT> ) {
    chomp;
    m%^\s*module\s+load\s+([^#]*)(\s*#.*)?\s*$% or next;
    $dependencies=$1;
    $dependencies =~ s%\/[^\s]*%%g;
    $dependencies =~ s%PrgEnv-[^\s]*%gcc%;
    $dependencies =~ s%\b$package\b%%;
    $dependencies =~ s%\s+% %g;
    $dependencies =~ s%^\s+%%;
    $dependencies =~ s%\s+$%%;

    map { $h{ $package }{ $_ } = $path } split( ' ', $dependencies ); 
  }
  close SCRIPT;
}

sub getPrerequisites {
  my @list = @_;
  my ( $package );
  foreach $package ( sort @list ) {
    defined( $h{ $package } ) or next;
    $seen{ $package } and next;
    print $package, ' => ', join( ', ', sort keys $h{ $package } ), "\n";
    map { $need{ $_ } = 1 } keys $h{ $package };
    $seen{ $package } = 1;
  }
  if ( scalar( keys %need ) > scalar( @list ) ) {
    getPrerequisites( keys %need );
  }
}

foreach ( @ARGV ) {
  if ( m%^\^(.*)$% ) {
    $package = $1;
    delete $h{ $package };
    map { delete $h{ $_ }{ $package } } keys %h;
  } else {
    push @args, $_;
  }
}
getPrerequisites( @args );
print "Prerequisites for ", join( ', ', sort @args ), ":\n";
print join( ' ', sort keys %need ), "\n";