#!/bin/bash

cd `dirname $0`/..

cat <<EOF

# List of module directories missing w.r.t build scripts
EOF

for tool in `ls -d  {usg,jgi}/*/*/build | awk -F/ '{ print $1"/"$3 }'`
do
  ls -ld modules/templates/$tool >/dev/null
done 2>&1 | sed -e 's%^.*modules/%modules/%' -e 's% .*$%%' | tr -d ':'
# done # 2>&1 | awk -F\' '{ print $2 }' | tr -d ':'

cat <<EOF

# List of build scripts missing w.r.t. module versions
EOF

for top in usg jgi
do
  for tool in `ls -d  modules/templates/$top/* | awk -F/ '{ print $NF }' | egrep -v 'PrgEnv-gnu|OFED'`
  do
    for vsn in `ls modules/templates/$top/$tool/`
    do
      ls $top/*/$tool/build/build-${tool}-${vsn}*.sh >/dev/null
    done
  done
done 2>&1 | sed -e "s%^[^ ]* %%" -e 's% .*$%%' | tr -d "':"
# done # 2>&1 | awk -F\' '{ print $2 }' | tr -d ':'
