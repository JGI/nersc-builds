_me="${BASH_SOURCE[0]}"
_currentdir="${_me%/*}"
if [ -d ${_currentdir} ]; then
  _real_currentdir=$(cd $_currentdir; pwd)
else
  _real_currentdir=${_currentdir}
fi

export PATH=${_real_currentdir}:/bin:/usr/bin:/usr/local/bin:/sbin:/usr/sbin:/usr/local/sbin

unset `env | grep PERL | awk -F= '{ print $1 }'`
unset LD_LIBRARY_PATH PYTHONPATH CFLAGS CPPFLAGS CXXFLAGS LDFLAGS
unset PREFIX NERSC_BUILD_MODULE_PREREQ

module purge
module unuse $MODULEPATH

if [ -z "$NERSC_BUILD_MODULE_INSTALL_ROOT" ]; then
  echo "NERSC_BUILD_MODULE_INSTALL_ROOT not defined, and that's bad..."
else
  module use $NERSC_BUILD_MODULE_INSTALL_ROOT/usg
  module use $NERSC_BUILD_MODULE_INSTALL_ROOT/jgi
fi
