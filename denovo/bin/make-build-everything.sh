#!/bin/bash
#
# This script takes the build-order from build-order.txt and spits out a script
# to build everything, in a clean environment.
#

cd `dirname $0`
install_root=$(cd ..; pwd)/Modules
cat <<-EOF
	#!/bin/bash

	export NERSC_BUILD_ALLOW_ANY_USER=true
	export NERSC_BUILD_NO_CLEAN_SOURCES=true
	export NERSC_BUILD_NO_FORCE_REBUILD=true
	export NERSC_BUILD_MODULE_INSTALL_ROOT=$install_root

	set -ex

	cd `pwd`
	. install-modules.sh
	. clean-environment.sh

	cd ..

EOF

for i in `cat build-order.txt | egrep -v '^$' | egrep -v '^#'`
do
  echo "bin/build-it.sh $i"
done
