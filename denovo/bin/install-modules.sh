#!/bin/bash

set -ex
if [ -z "$NERSC_BUILD_MODULE_INSTALL_ROOT" ]; then
  echo "NERSC_BUILD_MODULE_INSTALL_ROOT not defined, spitting the dummy..."
  exit 1
fi

HERE=`pwd`


if [ -z "$NERSC_BUILD_NO_FORCE_REBUILD" ]; then
  rm -rf   $NERSC_BUILD_MODULE_INSTALL_ROOT/{usg,jgi}
  mkdir -p $NERSC_BUILD_MODULE_INSTALL_ROOT/{usg,jgi}
fi

if [ -d $NERSC_BUILD_MODULE_INSTALL_ROOT/templates ]; then

  cd $NERSC_BUILD_MODULE_INSTALL_ROOT/templates

  for dir in `find . -type d -print`
  do
    mkdir -p $NERSC_BUILD_MODULE_INSTALL_ROOT/$dir
  done

  for file in `find . -type f -print | grep -v module-template`
  do
    echo $file
    cat $file | \
      sed -e "s%NERSC_BUILD_MODULE_INSTALL_ROOT%$NERSC_BUILD_MODULE_INSTALL_ROOT%g" | \
      tee $NERSC_BUILD_MODULE_INSTALL_ROOT/$file >/dev/null
  done

  cd $HERE

fi
