#!/bin/bash
#SBATCH --mail-user wildish@lbl.gov
#SBATCH --mail-type all
#SBATCH --time 3-00:00:00
#SBATCH --output /global/homes/w/wildish/slurm.denovo.%j.out
#SBATCH --error  /global/homes/w/wildish/slurm.denovo.%j.err
#SBATCH --workdir /scratch

set -ex
cd /scratch
mkdir -p $USER/build-denovo/$SLURM_JOBID
cd $USER/build-denovo/$SLURM_JOBID

git clone https://bitbucket.org/TWildish/nersc-builds.git
cd nersc-builds/denovo/bin

set -x
./build-everything.sh
