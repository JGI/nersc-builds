#!/bin/bash

cat <<EOF

 Remove all sources manually, except those in the git repository...

EOF

cd `dirname $0`/../

for file in `ls {usg,jgi}/*/*/*/* | \
  egrep '\.tar.gz$|\.tar.bz2$|\.tar.xz$|\.tar$|\.zip$|Anaconda.*\.sh' | \
  egrep -v 'mysql|hdf5'`
do
  echo $file
  rm -f $file
done
