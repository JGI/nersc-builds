#!/usr/bin/env perl
use strict;
use warnings;
use File::Basename;

my ( $file, %h, %g, $script, $path, $package, $group, $dependencies, %seen );

chdir( dirname $0 ) || die "Cannot chdir: $!\n";
foreach $file ( glob( "../{usg,jgi}/*/*/build/*.sh" ) ) {
  # print $file, "\n";
  $file =~ m%\.\./(([^/]+)/[^/]+/([^/]+)/build/)(.*)$%;
  $path = $1;
  $group = $2;
  $package = $3;
  $script = $path . $4;
  next if $package eq 'bupc';
  defined( $h{ $group }{ $package } ) or $h{ $group }{ $package } = {};
  $g{ $group }{ $package } = $path;

  open SCRIPT, "<../$script" or die "Open $script: $!\n";
  while ( <SCRIPT> ) {
    chomp;
    m%^\s*module\s+load\s+([^#]*)(\s*#.*)?\s*$% or next;
    $dependencies=$1;
    $dependencies =~ s%\/[^\s]*%%g;
    $dependencies =~ s%PrgEnv-[^\s]*%gcc%;
    $dependencies =~ s%\b$package\b%%;
    $dependencies =~ s%\s+% %g;
    $dependencies =~ s%^\s+%%;
    $dependencies =~ s%\s+$%%;

    map { $h{ $group }{ $package }{ $_ } = $path } split( ' ', $dependencies ); 
  }
  close SCRIPT;
}

#
# Now to find the build order. Start with usg stuff, walk through the array, finding items
# with no dependencies. Build them first, remove them from other items dependencies, and
# remove them from the package list. Repeat until done...
#
# Then do the same with jgi packages
#

#
# Need to break a loop with gnustuff/libffi, perl/mysql/postgresql, perl/curl/openssl/git...
sub doGroupOnce {
  my $group = shift;
  my ( $path, $package, $nDeps, $gotOne );
  $gotOne = 1;
  while ( $gotOne ) {
    $gotOne = 0;

    foreach $package ( sort keys $h{ $group } ) {
      $nDeps = scalar( keys $h{ $group }{ $package } );
      if ( ! $nDeps ) {
        $gotOne = 1;
        map { delete $h{ $group }{ $_ }{ $package } } keys $h{ $group };
        # foreach ( keys $h{ $group } ) {
        #   delete $h{ $group }{ $_ }{ $package };
        # }
        $path = $g{ $group }{ $package } || 'undef';
        print " ==> $group $package - $path\n";
        delete $h{ $group }{ $package };
        $seen{ $package } = 1;
      }
    }
  }
}

sub doGroupRecurse {
  my $group = shift;
  my ( $path, $package );
  my ( $max, $maxPackage, %f );

  doGroupOnce( $group );

  while ( scalar keys $h{ $group } ) {
    $max = 0;
    map { $f{ $_ } = 0 } keys $h{ $group };
    foreach $package ( reverse sort keys $h{ $group } ) {
      # Why did I reverse sort? It's a hack, to get Perl in
      # the maxPackage before OpenSSL, which it ties with.
      foreach ( keys $h{ $group }{ $package } ) {
        $f{ $_ }++;
        if ( $f{ $_ } > $max ) {
          $max = $f{ $_ };
          $maxPackage = $_;
        }
      }
    }

    print "# Break ties on $maxPackage\n";
    $seen{ $maxPackage } = 1;
    $path = $g{ $group }{ $maxPackage };
    print " ==> $group $maxPackage - ", $path, "prolog\n";
    map { delete $h{ $group }{ $_ }{ $maxPackage } } keys $h{ $group };
    delete $h{ $group }{ $maxPackage };
    doGroupOnce( $group );
    print " ==> $group $maxPackage - ", $path, "epilog\n";
  }
}

sub pruneDeps {
  my $group = shift;
  my ( $path, $package );
  foreach $package ( keys $h{ $group } ) {
    map { delete $h{ $group }{ $package }{ $_ } } keys %seen;
  }
}

doGroupRecurse( 'usg' );
pruneDeps( 'jgi' );
print "\n";
doGroupRecurse( 'jgi' );

print "\n# All done\n";