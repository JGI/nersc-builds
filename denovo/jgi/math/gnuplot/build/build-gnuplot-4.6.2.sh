#!/bin/bash

#
# One remaining issue: gnuplot claims libgd is built without freetype support.
# To check this, launch gnuplot, then 'set terminal png'. You'll see the following:
#
# gnuplot> set terminal png
# Terminal type set to 'png'
# libgd was not built with FreeType font support
#  when opening font "arial", using internal non-scalable font
# Options are 'nocrop enhanced size 640,480 medium '
#

cd `dirname $0`

prerequisites="libgd/2.2.4"
set -ex
module purge
module load PrgEnv-gnu/7.1
module load cmake/3.9.0
module load $prerequisites

vsn=4.6.2
dir=gnuplot-$vsn
file=gnuplot-$vsn.tar.gz
url=https://downloads.sourceforge.net/project/gnuplot/gnuplot/$vsn/$file
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

[ -f $file ] || wget --no-check-certificate -O $file $url
[ -d $dir ] && rm -rf $dir
tar xf $file
cd $dir
export LDFLAGS="$ZLIB_LIB $LIBPNG_LIB $LIBGD_LIB $LIBJPEG_LIB $LIBFREETYPE_LIB"
export CPPFLAGS="$LIBGD_INC"
#
# Using our readline library causes the docs to not build correctly
#./configure --prefix=$PREFIX --with-gd=$GD_DIR --with-readline=$READLINE_DIR
./configure --prefix=$PREFIX --with-gd=$GD_DIR --with-readline=builtin

export NPROCS=${NPROCS:=32}

make -j $NPROCS
make install-strip
cd ..
rm -r $dir

fix_perms -g usg $PREFIX
create-module $PREFIX "$prerequisites"
