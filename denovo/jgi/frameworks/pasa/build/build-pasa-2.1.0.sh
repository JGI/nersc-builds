#!/bin/bash

set -ex
cd `dirname $0`

module purge
module load PrgEnv-gnu/5.4 # Fails with 6.3.0!
module load perl/5.24.0
module load zlib/1.2.11
module load gnustuff/1.0 # for the patch utility

vsn=2.1.0
file=v$vsn.tar.gz
dir=PASApipeline-$vsn
url=https://github.com/PASApipeline/PASApipeline/archive/$file
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

[ -f $file ] || wget --no-check-certificate -O $file $url
[ -d $dir ] && rm -rf $dir
tar xf $file

export PASAHOME=$PREFIX
mkdir -p $PREFIX/pasa_conf/

cd $dir
patch -p0 < ../patch-2.1.0-zlib.txt
cd ..

mv $dir/* $PREFIX
cp conf.txt $PREFIX/pasa_conf/
rmdir $dir
pushd $PREFIX

export NPROCS=${NPROCS:=32}

make -j $NPROCS

fix_perms -g jgitools $PREFIX
create-module $PREFIX
