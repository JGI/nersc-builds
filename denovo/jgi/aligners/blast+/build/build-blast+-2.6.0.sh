#!/bin/bash

set -ex
cd `dirname $0`

prerequisites="libpng/1.6.28 pcre/8.40 libjpeg/6b"
module purge
module load PrgEnv-gnu/7.1
module load python/2.7-anaconda
module load "$prerequisites"

vsn=2.6.0
file=ncbi-blast-${vsn}+-src.tar.gz
url=ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/$vsn/$file
dir=ncbi-blast-${vsn}+-src
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
[ -f $file ] || wget -O $file $url

[ -d $dir ] && rm -rf $dir
tar zxf $file

cd $dir/c++
# used to have "-static" in the LDFLAGS, but that doesn't work now for some reason
./configure --prefix=$PREFIX \
	--with-bin-release \
	--with-mt \
	--with-64 \
	--without-debug \
	--with-optimization \
	LDFLAGS="$ZLIB_LIB -lz" \
	--with-static \
	--with-png=$LIBPNG_DIR \
	--with-jpeg=$LIBJPEG_DIR \
	--with-pcre=$PCRE_DIR \
	--with-python=$PYTHON_DIR

export NPROCS=${NPROCS:=32}

make -j $NPROCS \
  CFLAGS="$GCC_RECORD_SWITCHES" \
  CXXFLAGS="$GCC_RECORD_SWITCHES"

# make check doesnt do anything useful, unfortunately :-(
# make check

# make install has an error because include/common/ already exists, having
# been created at some point earlier in the install.
# just remove the duplicate first, it has nothing useful anyway.
rm -rf ReleaseMT/inc/common
make install

cd ../..
rm -r $dir
fix_perms -g jgitools $PREFIX
create-module $PREFIX "$prerequisites"
