#!/bin/bash

set -ex
cd `dirname $0`
module purge
module load PrgEnv-gnu/4.9 # Will build with 5.4, but don't want to use that
module load python/2.7-anaconda
module load perl/5.24.0

vsn=1969
file=sim4db-$vsn.tgz
url=https://svn.code.sf.net/p/kmer/code/
dir=kmer-$vsn
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
[ -d $dir ] && rm -rf $dir

#
# This is old code, no guarantee it's going to be around forever.
# Stash the source, just in case!
if [ -f $file ]; then
  tar xf $file
else
  svn co --revision $vsn $url $dir
  tar zcf $file $dir
fi

cd $dir/trunk
./configure.sh
make
make install
mv Linux-amd64 $PREFIX
cd ../..
rm -rf $dir
fix_perms -g jgi $PREFIX
