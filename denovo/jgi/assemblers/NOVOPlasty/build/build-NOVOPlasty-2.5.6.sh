#!/bin/bash

set -ex
cd `dirname $0`

vsn=2.5.6
file=$vsn.tar.gz
dir=NOVOPlasty-$vsn
url=https://github.com/ndierckx/NOVOPlasty/archive/$file
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget --no-check-certificate -O $file $url

mkdir -p $PREFIX
tar xf $file
cd $dir
cp * $PREFIX/
cd ..
rm -rf $dir
cd $PREFIX
ln -s NOVOPlasty$vsn.pl NOVOPlasty.pl
chmod a+x NOVOPlasty$vsn.pl

fix_perms -g usg $PREFIX
create-module $PREFIX
