#!/bin/bash

set -ex
cd `dirname $0`

export NPROCS=${NPROCS:=32}

module purge
module load PrgEnv-gnu/7.1
module load python/2.7-anaconda
module load git/2.11.0
module load openssl/1.1.0c

vsn=1.8.8
PREFIX=$(cd ..; pwd)/$vsn
file=`pwd`/FALCON-integrate.$vsn.tgz

[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
mkdir -p $PREFIX
cd $PREFIX

if [ ! -f $file ]; then
  git clone git://github.com/PacificBiosciences/FALCON-integrate.git $vsn
  cd $vsn
  git checkout $vsn
  git submodule update --init --recursive
 (cd ..; tar zcf $file $vsn)
fi

tar --strip-components=1 -xf $file
git checkout $vsn
git submodule update --init --recursive
make init
source env.sh
make config-edit-user
make -j $NPROCS all
make test

fix_perms -g jgitools $PREFIX
create-module $PREFIX/FALCON-integrate
