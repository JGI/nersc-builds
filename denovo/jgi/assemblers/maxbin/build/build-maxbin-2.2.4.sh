#!/bin/bash

set -ex
cd `dirname $0`
HERE=`pwd`
module purge
module load PrgEnv-gnu/5.4 # 6.x - 7.1 all fail :(
module load gnustuff/1.0
module load perl/5.24.0

export NPROCS=${NPROCS:=32}

vsn=2.2.4
file=MaxBin-$vsn.tar.gz
url=https://downloads.sourceforge.net/project/maxbin/$file
dir=MaxBin-$vsn
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

[ -d $dir ] && rm -rf $dir

[ -f $file ] || wget --no-check-certificate -O $file $url
tar xf $file
cd $dir
patch -p0 < ../patch-$vsn-buildapp
cd ..

mv $dir $PREFIX # build in-place - Sucky! 8-/
cd $PREFIX/src
make -j $NPROCS

cd ..
./autobuild_auxiliary
rm auxiliary/{*.tar.gz,*.zip} 2>/dev/null
find . -type f -exec strip {} \; 2>/dev/null

$HERE/fix-perl-shebang.sh

fix_perms -g jgitools $PREFIX
create-module $PREFIX
