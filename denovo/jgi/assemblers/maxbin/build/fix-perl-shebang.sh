#!/bin/sh

for file in `find . -type f -exec grep -l '/usr/bin/perl' {} \;`
do
  [ -f $file.orig ] || mv $file $file.orig
  cat $file | \
    sed \
      -e 's%/usr/bin/perl -w%/usr/bin/env perl\nuse warnings;%' \
      -e 's%/usr/bin/perl%/usr/bin/env perl%' | \
    tee $file >/dev/null
  chmod +x $file
  rm -f $file.orig
done
