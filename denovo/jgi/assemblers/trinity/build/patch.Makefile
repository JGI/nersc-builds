--- Makefile.orig	2017-10-25 19:20:44.101157649 +0000
+++ Makefile	2017-10-25 19:20:11.769525686 +0000
@@ -23,7 +23,7 @@
                 && sh ./configure --prefix=`pwd` $(INCHWORM_CONFIGURE_FLAGS) && $(MAKE) install
 
 chrysalis_target:
-	cd Chrysalis && $(MAKE) UNSUPPORTED=yes $(CHRYSALIS_MAKE_FLAGS)
+	cd Chrysalis && $(MAKE) UNSUPPORTED=yes $(CHRYSALIS_MAKE_FLAGS) CPPFLAGS="-I."
 
 
 trinity_essentials:
