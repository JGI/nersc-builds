#!/bin/bash

# important: must edit Makefile in trinity-plugins to remove -ltinfo

module purge
module load PrgEnv-gnu/7.1
module load zlib/1.2.11
module load gnustuff/1.0

set -ex
cd `dirname $0`

export NPROCS=${NPROCS:32}

hpcvsn=1.0.2
hpcfile=v${hpcvsn}.tar.gz
hpcurl=https://github.com/HpcGridRunner/HpcGridRunner/archive/$hpcfile

vsn=2.3.2
file=Trinity-v$vsn.tar.gz
dir=trinityrnaseq-Trinity-v$vsn
url=https://github.com/trinityrnaseq/trinityrnaseq/archive/$file
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget -O $file $url
mkdir -p $PREFIX

tar xf $file
cd $dir
patch -p0 < ../patch.Makefile
patch -p0 < ../patch.trinity-plugins.Makefile
make -j $NPROCS
make plugins
rsync -a ./ $PREFIX/
cd ..
rm -rf $dir

cd $PREFIX
wget -O $hpcfile $hpcurl
tar xf $hpcfile
rm $hpcfile

fix_perms -g jgi $PREFIX
create-module $PREFIX
