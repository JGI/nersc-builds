--- trinity-plugins/Makefile.orig	2017-10-25 18:50:31.142611957 +0000
+++ trinity-plugins/Makefile	2017-10-25 19:14:04.615229852 +0000
@@ -30,7 +30,7 @@
 
 samtools:
 	tar xvf samtools-0.1.19.tar.bz2
-	cd samtools-0.1.19 && $(MAKE) $(LTINFO)
+	cd samtools-0.1.19 && $(MAKE) $(LTINFO) CFLAGS="-g -Wall -O2 -I${ZLIB_DIR}/include -L${ZLIB_DIR}/lib" INCLUDES="-I${ZLIB_DIR}/include -I." LIBPATH="-L${ZLIB_DIR}/lib"
 	mv samtools-0.1.19/samtools ./BIN/.
 
 jellyfish:
@@ -41,7 +41,7 @@
 htslib_target:
 #	tar xjvf htslib-1.2.1.tar.bz2
 #	cd htslib-1.2.1 && ./configure && $(MAKE)
-	tar xvf ${HTSLIB_CODE} && cd htslib && $(MAKE)
+	tar xvf ${HTSLIB_CODE} && cd htslib && $(MAKE) CPPFLAGS="-I${ZLIB_DIR}/include" LDFLAGS="-L${ZLIB_DIR}/lib"
 
 scaffold_iworm_contigs_target: htslib_target
 	cd scaffold_iworm_contigs && $(MAKE)
