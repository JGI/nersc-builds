#!/bin/bash

cd `dirname $0`
echo "This is obsolete..."
exit 1

openmpi_version=2.1.1
version=2.26.0
prefix=$(cd ..; pwd)/
install=${prefix}/${version}

tar=berkeley_upc-${version}.tar.gz
url=http://upc.lbl.gov/download/release/${tar}

export NPROCS=${NPROCS:=32}

set -e
set -x

module load PrgEnv-gnu/7.1

builddir=$TMPDIR/build-bupc-${version}
[ -d ${builddir} ] || mkdir ${builddir}
[ -f $tar ] || wget $url
cd ${builddir}

tar -xvzf $prefix/build/$tar
cd berkeley_upc-${version}
mkdir -p build
cd build
cupc_install=$(cd ../..; pwd)/cupc/3.9.1-0-gnu7.1
if ! $cupc_install/bin/clang-upc2c --version
then
  echo "Please ensure that a valid clang-upc2c version exists at $cupc_install"
  exit 1
fi

slurmdir=$(which srun)
slurmdir=${slurmdir%/*/*}
pmi_conf="--with-pmi=$slurmdir"
CONF=" CUPC2C_TRANS=${cupc_install}/bin/clang-upc2c --with-multiconf=+dbg_cupc2c,+opt_cupc2c --disable-udp --enable-smp --with-sptr-packed-bits=19,11,34"
CONF_NOPSHM="$CONF --disable-pshm --disable-aligned-segments"
CONF_POSIX="--enable-pshm $CONF"
CONF_SYSV="$CONF --enable-pshm --disable-pshm-posix --enable-pshm-sysv"
CONF_FILE="$CONF --enable-pshm --disable-pshm-posix --enable-pshm-file"

buildAndInstall()
{
  pe=$1
  pever=$2

  module rm openmpi OFED
  p=${prefix}/${version}/${pe}/${pever}-smp
  if [ ! -x $p/bin/upcc ]
  then 
    module list
    make distclean 2>/dev/null || /bin/true
    echo "Building ${version}-${subver}-smp "
    ../configure CC=$(which gcc) CXX=$(which g++) $pmi_conf --disable-ibv --prefix=${p} $CONF_SYSV
    make -j $NPROCS || make 
    make install
  fi

  # load MPI / noibv
  module rm OFED
  module load openmpi/${openmpi_version}
  p=${prefix}/${version}/${pe}/${pever}-sysv
  if [ ! -x $p/bin/upcc ]
  then 
    module list
    make distclean 2>/dev/null || /bin/true
    echo "Building ${version}-${subver}-sysv "
    ../configure CC=$(which gcc) CXX=$(which g++) MPI_CC=mpicc $pmi_conf --prefix=${p} --disable-ibv $CONF_SYSV
    make -j $NPROCS || make 
    make install
  fi


#  # load MPI / inifinband
#  module load OFED
#  module load openmpi/${openmpi_version}
#  p=${prefix}/${version}/${pe}/${pever}-sysv-ibv
#  if [ ! -x $p/bin/upcc ]
#  then 
#    module list
#    make distclean 2>/dev/null || /bin/true
#    echo "Building ${version}-${subver}-sysv "
#    IBVHOME=$OFED_DIR ../configure CC=$(which gcc) CXX=$(which g++) MPI_CC=mpicc $pmi_conf --prefix=${p} $CONF_SYSV
#    make -j $NPROCS || make 
#    make install
#  fi

#  p=${prefix}/${version}/${pe}/${pever}-nopshm
#  if [ ! -x $p/bin/upcc ]
#  then
#    module list
#    make distclean 2>/dev/null || /bin/true
#    echo "Building ${version}-${subver}-nopshm "
#    IBVHOME=$OFED_DIR ../configure CC=$(which gcc) CXX=$(which g++) MPI_CC=mpicc $pmi_conf --prefix=${p} $CONF_NOPSHM
#    make -j $NPROCS || make 
#    make install
#  fi

#  p=${prefix}/${version}/${pe}/${pever}-posix
#  if [ ! -x $p/bin/upcc ]
#  then
#
#    make distclean 2>/dev/null || /bin/true
#    echo "Building ${version}-${subver}-posix "
#    IBVHOME=$OFED_DIR ../configure CC=$(which gcc) CXX=$(which g++) MPI_CC=mpicc $pmi_conf --prefix=${p} $CONF_POSIX
#    make -j $NPROCS || make 
#    make install
#  fi

}

module purge
module load PrgEnv-gnu/7.1
buildAndInstall gnu 7.1

if [ 1 == 0 ]
then
  module purge
  module load PrgEnv-gnu/4.8
  buildAndInstall gnu 4.8

  module load PrgEnv-gnu/5.4
  buildAndInstall gnu 5.4

  module purge
  module load PrgEnv-gnu/6.3
  buildAndInstall gnu 6.3
fi

cd ..
rm -rf berkeley_upc-${version}
cd $prefix/build

echo "Done"
