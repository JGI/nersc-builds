#!/bin/bash

cd `dirname $0`

module purge
module load PrgEnv-gnu/7.1
module load cmake/3.9.0
module load git/2.11.0
module load python/3.6-anaconda
module load openssl/1.1.0c

module list
which cmake

set -ex
VER=3.8.1-0
TAG=clang-upc-$VER
prefix=$(cd ..; pwd)

here=`pwd`
cd $prefix/build

[ -d llvm ] && rm -rf llvm
git clone https://github.com/Intrepid/llvm-upc.git llvm
cd llvm ; git fetch ; git checkout $TAG

cd tools
[ -d clang ] || git clone https://github.com/Intrepid/clang-upc.git clang
cd clang ; git fetch ; git checkout $TAG

cd tools;
[ -d upc2c ] || git clone https://github.com/Intrepid/upc2c.git upc2c
cd upc2c ; git fetch ; git checkout $TAG

buildit()
{
  mod=$1
  modver=$2
  modspec=$mod/$modver
  instname=${mod}${modver}
  module load PrgEnv-${modspec}
  
  build=$(mktemp --directory --tmpdir=`pwd`)
  cd $build

  prefix_ver=$prefix/${instname}/$VER
  [ -d $prefix_ver ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $prefix_ver ] && rm -rf $prefix_ver

  cmake $prefix/build/llvm -DCMAKE_INSTALL_PREFIX:PATH=$prefix_ver -DLLVM_TARGETS_TO_BUILD:=host -DCMAKE_BUILD_TYPE:=Release

  export NPROCS=${NPROCS:=32}

  make -j $NPROCS
  make install

  cd -
  rm -rf $build
  module rm PrgEnv-$modspec
}

buildit gnu 7.1
cd $here
rm -rf llvm
fix_perms -g jgitools $prefix
create-module $PREFIX
