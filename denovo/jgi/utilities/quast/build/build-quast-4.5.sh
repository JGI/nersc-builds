#!/bin/bash

set -ex
cd `dirname $0`

module purge
module load PrgEnv-gnu/7.1
module load cmake/3.9.0
module load boost/1.59.0
module load python/2.7-anaconda
module load gnustuff/1.0

vsn=4.5
file=quast_$vsn.tar.gz
url=https://github.com/ablab/quast/archive/$file
dir=quast-quast_$vsn # yeah, really...
[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir
tar xf $file

PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX
mkdir -p $PREFIX

cd $dir
patch -p0 < ../patch-GAGE-test-return.txt
cd ..

mv $dir/* $dir/.*ignore $PREFIX
cd $PREFIX

# e-mem needs to be compiled with boost
cd quast_libs/MUMmer/e-mem-src
export NPROCS=${NPROCS:=32}
make -j $NPROCS LDFLAGS="$GCC_RECORD_SWITCHES" CFLAGS="$GCC_RECORD_SWITCHES $BOOST_INC -std=gnu++0x -frecord-gcc-switches -static-libstdc++ -mtune=native -static-libgcc"
strip e-mem
mv e-mem ..

cd $PREFIX
python quast.py --test --debug
python metaquast.py --test --debug
./install_full.sh

rm -rf test_data
rm -rf quast_test_output
fix_perms -g jgitools $PREFIX
create-module $PREFIX
