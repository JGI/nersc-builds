#!/bin/bash

set -ex
cd `dirname $0`

module purge
module load PrgEnv-gnu/7.1
module load gnustuff/1.0

vsn=25.1
file=emacs-$vsn.tar.gz
dir=emacs-$vsn
url=http://mirrors.ocf.berkeley.edu/gnu/emacs/$file
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

[ -d $dir ] && rm -rf $dir
[ -f $file ] || wget --no-check-certificate -O $file $url
tar xf $file
cd $dir

export CFLAGS=$GCC_RECORD_SWITCHES
export CPPFLAGS="$GNUSTUFF_INC"
export LDFLAGS="$GCC_RECORD_SWITCHES $GNUSTUFF_LIB"
./configure --prefix=$PREFIX \
  --with-gif=no \
  --with-x-toolkit=no \
  --with-xpm=no \
  --with-jpeg=no \
  --with-png=no \
  --with-tiff=no

export NPROCS=${NPROCS:=32}

make -j $NPROCS
make check
make install
cd ..
rm -r $dir
fix_perms -g usg $PREFIX
create-module $PREFIX
