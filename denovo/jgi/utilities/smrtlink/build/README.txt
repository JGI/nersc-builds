For Genepool, configure as follows:

----- Part 2 of 7: SMRT Link Server DNS -----
gpint207.nersc.gov

----- Part 3 of 7: SMRT Link Setup ----
Enter the SMRT Link GUI (http) port [9090]:
Enter the SMRT Link Services port [9091]:
Enter the SMRT Link GUI Shutdown port [9092]:
Enter the SMRT Link GUI AJP port [9093]:

Enter the SMRT Link Services initial memory (in MB) [32768]: 16384
Enter the SMRT Link Services maximum memory (in MB) [32768]: 16384
Enter the SMRT Link GUI initial memory (in MB) [6528]:
Enter the SMRT Link GUI maximum memory (in MB) [6528]:

Enter the Services Event URL? []:
Enter the SMRT View Server (http) port [9094]:
Enter the initial memory SMRT View Server (in MB) [26240]: 13120
Enter the maximum memory SMRT View Server (in MB) [26240]: 13120

Enter the full path to the 'data_root' directory [data_root.local]: /global/seqfs/sdm/testing/pacbio/runs
# (use jobs5 for smrtlink v5)
Enter the full path to the 'jobs_root' directory [jobs_root.local]: /global/seqfs/sdm/testing/pacbio/jobs4
Enter the full path to the 'tmp_dir' directory [/tmp/smrtlink]: /global/projectb/scratch/smrt_dev/tmp

Pick an option:
1) SGE
2) Other JMS
3) None (Non-Distributed Mode)
Choice [1]:

Detected the following settings:
SGE_ROOT=/opt/uge/genepool/uge
SGE_CELL=genepool
SGE_BINDIR=/opt/uge/genepool/uge/bin/lx-amd64
Are these correct [Y/n]:

Select the queue to use for SMRT Analysis jobs:
Pick an option:
9) normal.q
env: 12-pe_slots

Additional arguments to the SGE job submission command may be
added in SGE_STARTARGS. The default job submission command is:

qsub -S /bin/bash -sync y -V -q $⁠{QUEUE} -N $⁠{JOB_NAME} \
-o $⁠{STDOUT_FILE} -e $⁠{STDERR_FILE} \
-pe $⁠{PE} $⁠{NPROC} $⁠{CMD}

Specify extra 'qsub' args, SGE_STARTARGS []:

Enter the max number of workers 'NWORKERS' [50]: 24
Enter the number of processors per task 'NPROC [31]: 14
Enter the total number of processors 'TOTAL_NPROC' [1000]: 140
Enable chunking 'CHUNKING'? [Y/n]:
Enter the max number of chunks 'MAXCHUNKS' [24]: 
