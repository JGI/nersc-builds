#!/bin/bash
set -ex
cd `dirname $0`

VER=1.4
for pkg in samtools bcftools htslib
do
  TARBALL=$pkg-$VER.tar.bz2
  URL=https://github.com/samtools/$pkg/releases/download/$VER/$TARBALL
  if [ ! -e $TARBALL ]; then
    wget --no-check-certificate -O $TARBALL "$URL"
  fi
done
BASE_PREFIX=$(cd `dirname $0`/..; pwd)

function build_library {
  compiler=$1
  compiler_version=$2
  version=$3
  CC=$4
  echo " "
  echo "Start building $version for $CC $compiler_version"

  module purge
  module load PrgEnv-$compiler/$compiler_version
  module load xz/5.2.3 zlib/1.2.11 bzip2/1.0.6 curl/7.52.1 gsl/2.3
  module load gnustuff/1.0
  module load openssl/1.1.0c
  module list

  PREFIX=$BASE_PREFIX/$compiler$compiler_version/${version}

  [ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && return
  [ -d $PREFIX ] && rm -rf $PREFIX
  
  mkdir -p $PREFIX/{bin,lib,include/bam,share/man/man1}

  export CFLAGS="$GCC_RECORD_SWITCHES"
  export CPPFLAGS="$OPENSSL_INC $GSL_INC $BZIP2_INC $ZLIB_INC $XZ_INC $CURL_INC $GNUSTUFF_INC"
  export LDFLAGS="$GCC_RECORD_SWITCHES $OPENSSL_LIB $GSL_LIB $BZIP2_LIB $ZLIB_LIB $XZ_LIB $CURL_LIB $GNUSTUFF_LIB $OPENSSL_LIB"
  export LIBS="$XZ_DIR/lib/liblzma.a $BZIP2_DIR/lib/libbz2.a"

  export NPROCS=${NPROCS:=32}

# htslib:
  pkg=htslib
  htsdir=$pkg-$version
  [ -d $pkg-$version ] && rm -rf $pkg-$version
  tar xf $pkg-$version.tar.bz2
  cd $pkg-$version

  ./configure \
          --enable-libcurl \
          --prefix=$PREFIX

  make -j $NPROCS
  make install

  cd ..
# rm -rf $pkg-$version # Not so fast! Re-use it in the bcftools build

# bcftools
  pkg=bcftools
  [ -d $pkg-$version ] && rm -rf $pkg-$version
  tar xf $pkg-$version.tar.bz2
  cd $pkg-$version
  export HTSLIB_CPPFLAGS=$CPPFLAGS
  make -j $NPROCS CC=$CC \
          CFLAGS="$CFLAGS" \
          CPPFLAGS="$CPPFLAGS -I$PREFIX/include" \
          LDFLAGS="$LDFLAGS -L$PREFIX/lib" \
          HTSDIR="../$htsdir" \
          HTSLIB_LDFLAGS="$LDFLAGS" \
          USE_GSL=1 GSL_LIBS="-lgsl -lgslcblas" \
          prefix=$PREFIX \
          all install

  cd ..
  rm -rf $pkg-$version

# samtools

  pkg=samtools
  [ -d $pkg-$version ] && rm -rf $pkg-$version
  tar xf $pkg-$version.tar.bz2
  cd $pkg-$version
  
  ./configure \
    --enable-libcurl \
    --with-htsdir="../$htsdir" \
    --prefix=$PREFIX

  make -j $NPROCS all
  make install

# Joel claims these may be obsolete (INC0100456)
#  install misc/*.java misc/*.lua $PREFIX/bin
#  install libbam.a $PREFIX/lib
#  install *.h $PREFIX/include/bam

  cd ..
  rm -rf $pkg-$version
  rm -rf $htsdir

  fix_perms -g jgitools $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_library gnu $env $VER gcc
done
create-module $PREFIX
