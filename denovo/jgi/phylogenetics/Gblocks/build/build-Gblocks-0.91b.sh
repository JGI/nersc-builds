#!/bin/bash

cd `dirname $0`

set -ex
module purge

vsn=0.91b
file=Gblocks_Linux64_${vsn}.tar
url=http://molevol.cmima.csic.es/castresana/Gblocks/$file
dir=Gblocks_$vsn/
PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && [ -n "$NERSC_BUILD_NO_FORCE_REBUILD" ] && exit 0
[ -d $PREFIX ] && rm -rf $PREFIX

if [ ! -f $file ] && [ ! -f $file.Z ]; then
  wget -O $file.Z $url.Z
  gunzip $file.Z
fi

[ -d $dir ] && rm -rf $dir
mkdir -p $PREFIX

tar xf $file
mv $dir/* $PREFIX
rmdir $dir
fix_perms -g jgitools $PREFIX
create-module $PREFIX
