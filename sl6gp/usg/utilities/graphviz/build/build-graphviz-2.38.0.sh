#!/bin/bash

module purge
module load PrgEnv-gnu/4.6
module load qt/4.6.4
module load gts/0.7.6
module load autoconf/2.69
module load pcre/8.32
module load python/2.7.4
module load perl/5.16.0

set -ex
vsn=2.38.0

NPROCS=${NPROCS:=16}

PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && rm -rf $PREFIX

export BASE_DEPENDENCIES_CFLAGS="-I$PREFIX/include"
export BASE_DEPENDENCIES_LIBS="-L$PREFIX/lib -L$PREFIX/lib64"
export LIBFFI_LIBS="-L$PREFIX/lib -L$PREFIX/lib64 -lffi"
export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig

#
# libffi
ffivsn=3.2.1
ffifile=v$ffivsn.tar.gz
ffiurl=https://github.com/libffi/libffi/archive/$ffifile
ffidir=libffi-$ffivsn
[ -f $ffifile ] || wget -O $ffifile $ffiurl
[ -d $ffidir ] && rm -rf $ffidir
tar xf $ffifile
cd $ffidir
./autogen.sh
./configure --prefix=$PREFIX
# Fool Make into thinking it doesn't need to run 'makeinfo', which we don't have
touch doc/libffi.info
make -j $NPROCS
make install
cd ..
rm -rf $ffidir

#
# glib
gvsn_short=2.54
gvsn=${gvsn_short}.0
gfile=glib-$gvsn.tar.xz
gdir=glib-$gvsn
gurl=https://ftp.gnome.org/pub/gnome/sources/glib/$gvsn_short/$gfile
[ -d $gdir ] && rm -rf $gdir
[ -f $gfile ] || wget -O $gfile $gurl
tar xf $gfile
cd $gdir
./configure --prefix=$PREFIX \
    --enable-libmount=no \
    --with-pcre=internal

make -j $NPROCS
make install
cd ..
rm -rf $gdir

#
# gdk-pixbuf
pbvsn_short=2.36
pbvsn=${pbvsn_short}.10
pbfile=gdk-pixbuf-$pbvsn.tar.xz
pbdir=gdk-pixbuf-$pbvsn
pburl=https://ftp.gnome.org/pub/gnome/sources/gdk-pixbuf/$pbvsn_short/$pbfile
[ -d $pbdir ] && rm -rf $pbdir
[ -f $pbfile ] || wget -O $pbfile $pburl
tar xf $pbfile
cd $pbdir

export CFLAGS="-I$PREFIX/include -I$PREFIX/include/glib-2.0 -I$PREFIX/lib/glib-${gvsn_short}/include"
export PKG_CONFIG_PATH=$PREFIX/lib/pkgconfig
export LD_LIBRARY_PATH=$PREFIX/lib:$LD_LIBRARY_PATH

./configure --prefix=$PREFIX \
    --without-libtiff
make -j $NPROCS
make install
cd ..
rm -rf $pbdir

file=graphviz-$vsn.tar.gz
dir=graphviz-$vsn
url=http://www.graphviz.org/pub/graphviz/stable/SOURCES/$file
[ -d $dir ] && rm -rf $fdir
[ -f $file ] || wget -O $file $url

tar xf $file
cd $dir
./configure --prefix=$PREFIX \
  --enable-static=yes \
  --enable-shared=no \
  --enable-tcl=no

make -j $NPROCS
make install
cd ..
rm -rf $dir

fix_perms -g usg $PREFIX
