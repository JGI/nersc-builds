#!/bin/bash

module purge
module load PrgEnv-gnu/5.4
module load autoconf/2.69

set -ex
vsn=3.2.1
file=v$vsn.tar.gz
url=https://github.com/libffi/libffi/archive/$file
dir=libffi-$vsn
PREFIX=$(cd ..; pwd)/$vsn

[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir
tar zxf $file
cd $dir

./autogen.sh
./configure --prefix=$PREFIX
# Fool Make into thinking it doesn't need to run 'makeinfo', which we don't have
touch doc/libffi.info
make -j $NPROCS
make install
cd ..
rm -rf $dir
fix_perms -g usg $PREFIX
