#!/bin/bash

set -ex
vsn=2.5.6
file=$vsn.tar.gz
dir=NOVOPlasty-$vsn
url=https://github.com/ndierckx/NOVOPlasty/archive/$file
PREFIX=/usr/common/jgi/assemblers/NOVOPlasty/$vsn

[ -d $dir ] && rm -rf $dir
[ -d $PREFIX ] && rm -rf $PREFIX
[ -f $file ] || wget -O $file $url

mkdir -p $PREFIX
tar xf $file
cd $dir
cp * $PREFIX/
cd ..
rm -rf $dir
cd $PREFIX
ln -s NOVOPlasty$vsn.pl NOVOPlasty.pl
chmod a+x NOVOPlasty$vsn.pl

fix_perms -g usg $PREFIX
