#!/bin/bash -l

module purge
module load PrgEnv-gnu/6.3
module load cmake/3.4.3

set -ex

vsn=3.10.1
export PREFIX=$(cd ..; pwd)/$vsn
file=SPAdes-$vsn.tar.gz
url=http://spades.bioinf.spbau.ru/release$vsn/$file
dir=SPAdes-$vsn
[ -f $file ] || wget -q -O $file $url
[ -d $dir ] && rm -rf $dir

echo Using PREFIX=$PREFIX
[ -d $PREFIX ] && rm -rf $PREFIX
mkdir -p $PREFIX

tar xf $file
cd $dir

./spades_compile.sh \
    -DCMAKE_CXX_FLAGS="-frecord-gcc-switches" \
    -DCMAKE_C_FLAGS="-frecord-gcc-switches" \
    -DSPADES_STATIC_BUILD:BOOL=ON

cd build_spades
make install/strip
cd ../..
rm -r $dir
chmod -R a+rX,go-w $PREFIX
