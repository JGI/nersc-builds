#!/bin/bash

set -ex
module purge
module load PrgEnv-gnu/5.4

vsn=2.2.4
file=MaxBin-$vsn.tar.gz
url=https://downloads.sourceforge.net/project/maxbin/$file
dir=MaxBin-$vsn
PREFIX=$(cd ..; pwd)/$vsn

[ -d $dir ] && rm -rf $dir
[ -d $PREFIX ] && rm -rf $PREFIX

[ -f $file ] || wget --no-check-certificate -O $file $url
tar xf $file
cd $dir
patch -p0 < ../patch-$vsn-buildapp
cd ..

mv $dir $PREFIX # build in-place - Sucky! 8-/
cd $PREFIX/src
make -j 16

cd ..
./autobuild_auxiliary
rm auxiliary/{*.tar.gz,*.zip} 2>/dev/null
find . -type f -exec strip {} \; 2>/dev/null

fix_perms -g jgitools $PREFIX
