#!/bin/bash -l

set -ex
module purge
module load PrgEnv-gnu/6.3
module load capnproto/0.5.3
module load boost/1.59.0
module load zlib/1.2.8

vsn=1.1.1
file=v$vsn.tar.gz
url=https://github.com/marbl/Mash/archive/$file
dir=Mash-$vsn
PREFIX=$(cd ..; pwd)/$vsn

[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir
[ -d $PREFIX ] && rm -rf $PREFIX

tar xf $file
cd $dir

export CXXFLAGS="-I $ZLIB_DIR/include"
export LDFLAGS="-L $ZLIB_DIR/lib"
./bootstrap.sh
./configure --prefix=$PREFIX \
  --with-boost=$BOOST_DIR \
  --with-capnp=$CAPNPROTO_DIR
make -j 32
make install

cd ..
rm -rf $dir
fix_perms -g jgitools $PREFIX
