#!/bin/bash

set -e
module purge
module load gcc/5.4.0 # Fails with 6.3.0!
module load perl/5.22.0

set -x
vsn=2.1.0
file=v$vsn.tar.gz
dir=PASApipeline-$vsn
url=https://github.com/PASApipeline/PASApipeline/archive/$file
PREFIX=/usr/common/jgi/frameworks/pasa/$vsn

[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir
[ -d $PREFIX ] && rm -rf $PREFIX
tar xf $file

export PASAHOME=$PREFIX
mkdir -p $PREFIX/pasa_conf/
mv $dir/* $PREFIX
cp conf.txt $PREFIX/pasa_conf/
rmdir $dir
pushd $PREFIX
make -j 4

fix_perms -g jgitools $PREFIX
