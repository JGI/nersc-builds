#!/bin/bash

module purge
module load PrgEnv-gnu
module load python
VERSION=1919
PREFIX=/global/common/genepool/jgi/aligners/sim4db/$VERSION

if [ -e $PREFIX ]; then
    rm -r $PREFIX
fi
if [ -e kmer ]; then
    chmod -R u+w kmer
    rm -r kmer
fi

svn co https://svn.code.sf.net/p/kmer/code/trunk kmer
cd kmer
svn up -r$VERSION
make clean
make
make install
mv Linux-amd64 $PREFIX

cd ..
chmod -R u+w kmer
rm -r kmer
fix_perms -g usg $PREFIX/..
