#!/bin/bash -l
set -ex

VER=1.4
for pkg in samtools bcftools htslib
do
  TARBALL=$pkg-$VER.tar.bz2
  URL=https://github.com/samtools/$pkg/releases/download/$VER/$TARBALL
  if [ ! -e $TARBALL ]; then
    wget -O $TARBALL "$URL"
  fi
done
BASE_PREFIX=$(cd `dirname $0`/..; pwd)

function build_library {
  compiler=$1
  compiler_version=$2
  version=$3
  CC=$4
  echo " "
  echo "Start building $version for $CC $compiler_version"

  set +x
  module purge
  module load PrgEnv-$compiler/$compiler_version
  module load xz curl gsl/2.3
  module list
  set -x

  PREFIX=$BASE_PREFIX/$compiler$compiler_version/${version}

  if [ -e $PREFIX ]; then
    rm -rf $PREFIX
  fi
  
  mkdir -p $PREFIX/{bin,lib,include/bam,share/man/man1}

  export CPPFLAGS="-I$GSL_DIR/include -I$HTSLIB_DIR/include -I$XZ_DIR/include -I$CURL_DIR/include"
  export LDFLAGS="-L$GSL_DIR/lib -L$HTSLIB_DIR/lib -L$XZ_DIR/lib -L$CURL_DIR/lib"
  export LIBS="-llzma"

# htslib:
  pkg=htslib
  [ -d $pkg-$version ] && rm -rf $pkg-$version
  tar xf $pkg-$version.tar.bz2
  cd $pkg-$version

  ./configure \
          --enable-libcurl \
          --prefix=$PREFIX

  make -j 16
  make install

  cd ..
  rm -r $pkg-$version

# bcftools
  pkg=bcftools
  [ -d $pkg-$version ] && rm -rf $pkg-$version
  tar xf $pkg-$version.tar.bz2
  cd $pkg-$version
  make -j 16 CC=$CC USE_GPL=1 \
          CPPFLAGS="$CPPFLAGS" \
          LDFLAGS="$LDFLAGS" \
          GSL_LIBS="-lgsl -lgslcblas"
  make CC=$CC prefix=$PREFIX install

  cd ..
  rm -r $pkg-$version

# samtools
  pkg=samtools
  [ -d $pkg-$version ] && rm -rf $pkg-$version
  tar xf $pkg-$version.tar.bz2
  cd $pkg-$version
  
  ./configure \
    --enable-libcurl \
    --prefix=$PREFIX

  make -j 16 all
  make install

  install misc/*.java misc/*.lua $PREFIX/bin
  install libbam.a $PREFIX/lib
  install *.h $PREFIX/include/bam

  cd ..
  rm -r $pkg-$version

  chmod -R a+rX $PREFIX
  chgrp -R jgitools $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_library gnu $env $VER gcc
done
#build_library intel 13 $VER icc
