#!/bin/bash

set -ex

vsn=5.0.0.6792
file=smrtlink_$vsn.zip
url=https://downloads.pacbcloud.com/public/software/installers/$file
run=smrtlink_$vsn.run

if [ "$USER" != "smrt" ]; then
  echo "Please install this as the 'smrt' user"
  exit 0
fi

PREFIX=$(cd ..; pwd)/$vsn
[ -d $PREFIX ] && rm -rf $PREFIX

[ -f $file ] || wget -O $file $url
[ -f $run ] && rm -f $run
[ -f $run.md5 ] && rm -f $run.md5

unzip -P jeip5Eex $file
./$run --install --rootdir $PREFIX

rm $run{,.md5}
fix_perms -g usg $PREFIX
