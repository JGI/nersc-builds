#!/bin/bash

set -ex

vsn=4.0.0.190159
file=smrtlink_$vsn.zip
url=https://downloads.pacbcloud.com/public/software/installers/$file
run=smrtlink_$vsn.run

PREFIX=/usr/common/jgi/utilities/smrtlink/$vsn
#[ -d $PREFIX ] && rm -rf $PREFIX

#[ -f $file ] || wget -O $file $url
#[ -f $run ] && rm -f $run
#[ -f $run.md5 ] && rm -f $run.md5

#unzip -P SmrT3chN $file
./$run --install --rootdir $PREFIX

rm $run{,.md5}
fix_perms -g usg $PREFIX
