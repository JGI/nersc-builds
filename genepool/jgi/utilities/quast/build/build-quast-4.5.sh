#!/bin/bash -l

module purge
module load PrgEnv-gnu/6.3
module load cmake/3.4.3
module load boost/1.59.0
module load python/2.7.4

set -e

vsn=4.5
file=quast_$vsn.tar.gz
url=https://github.com/ablab/quast/archive/$file
dir=quast-quast_$vsn # yeah, really...
[ -f $file ] || wget -O $file $url
[ -d $dir ] && rm -rf $dir
tar xf $file

PREFIX=$(cd ..; pwd)/$vsn
echo $PREFIX
[ -d $PREFIX ] && rm -rf $PREFIX
mkdir -p $PREFIX
mv $dir/* $dir/.*ignore $PREFIX
cd $PREFIX

# e-mem needs to be compiled with boost
cd quast_libs/MUMmer/e-mem-src
make CFLAGS="-I$BOOST_DIR/include -std=gnu++0x -frecord-gcc-switches -static-libstdc++ -mtune=native -static-libgcc"
strip e-mem
mv e-mem ..

cd $PREFIX
python quast.py --test --debug
python metaquast.py --test --debug
./install_full.sh

rm -rf test_data
rm -rf quast_test_output
chmod -R a+rX,a-w $PREFIX
