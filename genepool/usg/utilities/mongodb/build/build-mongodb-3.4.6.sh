#!/bin/bash

module purge
module load modules PrgEnv-gnu/5.4 python/2.7.4 scons/2.5.0

vsn=3.4.6
file=mongodb-src-r$vsn.tar.gz
url=http://fastdl.mongodb.org/src/$file
dir=mongodb-src-r$vsn

set -ex
if [ ! -f $file ]; then
  wget -O $file $url
fi
if [ -d $dir ]; then
  rm -rf $dir
fi

tar zxf $file
cd $dir
patch -p0 <../patch.mongodb.$vsn
scons --jobs=16 all CC=`which gcc` CXX=`which g++`

PREFIX=/usr/common/usg/utilities/mongodb/$vsn
#scons --jobs=6 --nostrip=0i --PREFIX=$PREFIX install CC=`which gcc` CXX=`which g++`

mkdir -p $PREFIX/bin
cp distsrc/{THIRD-PARTY-NOTICES,GNU-AGPL-3.0,LICENSE.OpenSSL,MPL-2,README} $PREFIX
cp mongo{,bridge,d,perf,s} $PREFIX/bin/
chmod -R 775 $PREFIX
strip $PREFIX/bin/mongo*

cd ..
rm -rf $dir $file

fix_perms -g usg $PREFIX

echo "*"
echo "*"
echo "* Now run build-$vsn.tools.sh"
echo "*"
echo "*"
