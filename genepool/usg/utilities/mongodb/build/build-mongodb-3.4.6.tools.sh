#!/bin/bash
#
# mongo tools (mongorestore etc) are now split out into a separate repository
# (https://github.com/mongodb/mongo-tools)
#

module purge
module load modules golang

vsn=3.4.6
file=mongodb-tools-r$vsn.tar.gz
url=https://github.com/mongodb/mongo-tools/archive/r$vsn.tar.gz
dir=mongo-tools-r$vsn

set -ex
if [ ! -f $file ]; then
  echo wget -Q -O $file $url
  wget -O $file $url
fi
if [ -d $dir ]; then
  rm -rf $dir
fi

tar -zxf $file
cd $dir

mkdir bin
. ./set_gopath.sh
for f in bsondump mongodump mongoexport mongofiles mongoimport mongooplog mongorestore mongostat mongotop; do
  go build -o bin/$f -tags ssl $f/main/$f.go
done

PREFIX=/usr/common/usg/utilities/mongodb/$vsn
strip bin/*
mv bin/* $PREFIX/bin/

cd ..
rm -rf $dir $file
fix_perms -g usg $PREFIX
