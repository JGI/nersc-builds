#!/bin/bash -l
set -ex

VER=2.3
TARBALL=gsl-$VER.tar.gz
URL=http://gnu.mirror.constant.com/gsl/$TARBALL
BASE_PREFIX=$(cd `dirname $0`/..; pwd)

if [ ! -e $TARBALL ]; then
	wget -O $TARBALL "$URL"
fi

function build_library {
	compiler=$1
	compiler_version=$2
	version=$3
	CC=$4
	echo " "
	echo "Start building $version for $CC $compiler_version"

	module purge
	module load PrgEnv-$compiler/$compiler_version
	module list

	PREFIX=$BASE_PREFIX/$compiler$compiler_version/${version}

	if [ -e $PREFIX ]; then
		rm -rf $PREFIX
	fi
	
	mkdir -p $PREFIX/{bin,lib,include/bam,share/man/man1}

	[ -d gsl-$version ] && rm -rf gsl-$version
	tar xf $TARBALL
	cd gsl-$version
	
	./configure --prefix=$PREFIX

	make -j 30
	make check
	make install

	cd ..
	rm -rf gsl-$version

	fix_perms -g usg $PREFIX
#	chmod -R a+rX $PREFIX
#	chgrp -R jgitools $PREFIX
}

for env in `module avail PrgEnv-gnu --long 2>&1 | \
    egrep ^PrgEnv | \
    cut -f2 -d/ | \
    cut -f1 -d\ `
do
  build_library gnu $env $VER gcc
done
#build_library intel 13 $VER icc
