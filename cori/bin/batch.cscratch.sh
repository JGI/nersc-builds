#!/bin/bash
#SBATCH --constraint haswell
#SBATCH --qos=jgi
#SBATCH --time 2-00:00:00
#SBATCH --mail-user=wildish@lbl.gov
#SBATCH --mail-type=ALL
#SBATCH -N 1
#SBATCH -c 64
#SBATCH --output /global/cscratch1/sd/wildish/nersc-builds/cori/bin/slurm-build.cscratch.%j.out
#SBATCH --error  /global/cscratch1/sd/wildish/nersc-builds/cori/bin/slurm-build.cscratch.%j.err
#SBATCH --exclusive

cd $CSCRATCH

env | grep SLURM | sort

mkdir -p batch/$SLURM_JOBID
cd batch/$SLURM_JOBID

git clone https://bitbucket.org/TWildish/nersc-builds.git
cd nersc-builds/denovo/bin

./make-build-everything.sh | tee doit.sh
chmod +x doit.sh
./doit.sh
