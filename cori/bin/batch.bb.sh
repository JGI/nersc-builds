#!/bin/bash
#SBATCH --constraint haswell
#SBATCH --qos=jgi
#SBATCH --time 2-00:00:00
#SBATCH --mail-user=wildish@lbl.gov
#SBATCH --mail-type=ALL
#SBATCH -N 1
#SBATCH -c 64
#SBATCH --output /global/cscratch1/sd/wildish/nersc-builds/cori/bin/slurm-build.bb.%j.out
#SBATCH --error  /global/cscratch1/sd/wildish/nersc-builds/cori/bin/slurm-build.bb.%j.err
#SBATCH --exclusive
#DW jobdw capacity=200GB access_mode=private type=scratch

cd $DW_JOB_PRIVATE

env | grep SLURM | sort

[ -d nersc-builds ] && rm -rf nersc-builds
git clone https://bitbucket.org/TWildish/nersc-builds.git
cd nersc-builds/denovo/bin

./make-build-everything.sh | tee doit.sh
chmod +x doit.sh
./doit.sh
